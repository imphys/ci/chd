% This code loads a dataset from the '/data' directory 
% which should be a cell array of structures called 
% subParticles with localizations in the 'point' field 
% [x,y] and SQUARED uncertainties in 'sigma' field.The estimated size of the NPC is 
% stored in 'size' field:
% 
% subParticles{1}.points  -> localization data (x,y) in camera pixel units
% subParticles{1}.sigma   -> localization uncertainties (sigma) in SQUARED pixel units
% subParticles{1}.Height   ->  Particle Height (Tetra Height) in nm
%
% The following code will load the data from file and 
% then performs 4 steps to detect continous structural heterogeneity.
% Different example datasets are provided, both experimental as simulated data. 
% You only need to provide the 'dataset' name, and optionaly the values for
% K (number of neighbours used in Isomap).
% 
%
% The code makes use of the parallel computing toolbox 
% to distribute the load over different workers. 
% 
% (C) Copyright 2023               Quantitative Imaging Group
%     All rights reserved          Faculty of Applied Physics
%                                  Delft University of Technology
%                                  Lorentzweg 1
%                                  2628 CJ Delft
%                                  The Netherlands
%
% Sobhan Haghparast, 2023

addpath(genpath('MATLAB'))
addpath(genpath('data'))
addpath(genpath('Functions'))
path_mex_matlab1 = genpath('build/mex/');
path_mex_matlab2 = genpath('build/figtree/src/mex/');
path_matlab = genpath('MATLAB');

addpath(path_mex_matlab1)
addpath(path_mex_matlab2)
addpath(path_matlab)

load('3D_Tetra.mat')  % Loading the data and making output directory 
N=length(subParticles);
outdir='output/3D_Tetra'
            mkdir(outdir);

% Parameter initialization
CCD_pixel=130;              
USE_GPU_GAUSSTRANSFORM = 1;   
USE_GPU_EXPDIST = 1;
initAng = 'grid_72.qua';
scale = 0.04;               % Scale parameter used to calculate pairwise distances between particles
nIterations = 5;            % number of lie-algebra avg and consistency check                           % iterations
flagVisualizeSijHist = 0;   % show S_ij histogram (boolean) 
threshold = 0.5;            % consistency check threshold.

disp('Step 1: Pairwise registration of particles')


[RR, I, Values] = all2all3D(subParticles(1,1:300), scale, initAng, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST);
[MatrixAfterAll2all, MatrixAfterAll2all_norm] = MakeMatrix3Dn(Values, subParticles);
D = MatrixAfterAll2all_norm+MatrixAfterAll2all_norm';
D=Values+Values';
D = max(D(:))-D; 
D = D - diag(diag(D));      % Creating dissimilarity matrix 
disp('Step 2: Multi dimensional scaling space representation')

mds = mdscale(D,30,'Criterion','metricstress');       % Using multi dimensional scaling space

    for i=1:N
       Height(i)=subParticles{1,i}.Height;    % Extracting the Heights from simulated particles
    end
scatter3(mds(:,1),mds(:,2),mds(:,3),30,Height,''),title('Multi dimensional scaling space') %-- MDS Space--
colormap(jet)
a=colorbar;
a.Label.String = 'Height (nm)';

%% Unrolling data using ISOMAP
disp('Step 3: Unrolling data using Isomap and PCA to create latent space')

addpath (genpath('/Functions/drtoolbox'))  %% Adds drtoolbox to matlab path 
k=6;                                                        %% Number of neighbours used in Isomap
[mappedX, mapping] = isomap(mds, size(mds,2), k);           %% Isomap
figure,scatter3(mappedX(:,1),mappedX(:,2),mappedX(:,3),30,Height, 'filled');,title('Unrolled data using ISOMAP') %% Isomap
colormap(jet)
a=colorbar;
a.Label.String = 'Height (nm)';
% Creating the latent space using principal component analysis 
[U S V]=svd(mappedX); 
Latent_space=U*S(:,1)*transpose(V(1,1));
figure,scatter(Latent_space,zeros(1,length(Latent_space)),70,Height,'o')  % plots the latent space
colormap(jet)
a=colorbar;
a.Label.String = 'Height (nm)';
hold on
figure, histogram(Latent_space,50,'FaceColor','c'),title('Latent space and Histogram') % plots the histogram of the latent space

svd_mapped=svd(mappedX);
figure,scatter(1:1:30,svd_mapped,50,'filled'),title('SVD of the mapped data'), xlabel('dimension'),ylabel('SVD') % Plots the eigen value vs dimensions
rmpath (genpath('/Functions/drtoolbox'));
[coeff,score,latent,tsquared,explained] = pca(mappedX);
figure,bar(explained,1),title('Variance explained'),xlabel('Dimensions'),ylabel('Variance explained') % Plots the variance explained on each axis 
% subplot(3,3,7), biplot(coeff(:,1:3),'Scores',score(:,1:3));
[out,idx] = sort(Latent_space); % Takes the new order of the particles based on the latent space

%% dividing to register
% In case you choose joint registeration of multiple point cloud
% registeration for the reconstruction it requires to assign Mex files as
% explained in Readme file
disp('Step 4: Binning particles and reconstruction per bin')

Number_of_bins=6; % Choose number of bins 
[bins]=Isopca_bins(subParticles,Number_of_bins,idx); % divides bins with equal particles in each bin 

    % Registeration per bin
    for i=1:size(bins,1)
        clear registered
        particles_chunks(1,1:size(bins,2))=bins(i,1:size(bins,2));
        registered(1,:)=function_JRMPC3D(particles_chunks);
        registered_bin{i,:}=registered(1,:)
        
    end

    % Creating super Particle 
    for i=1:Number_of_bins
        ctemp=bins(i,:);
        ctemp=ctemp(~cellfun('isempty',ctemp));
        registered_bin_cloud{1,i}=par2cloud(ctemp,1:length(ctemp));
    end

%% Figures
% Plots each bin in scatter plot 
figure;
color=jet(100);
    for i=1:6
        subplot(2,3,i),scatter3(registered_bin_cloud{1,i}(:,1)*CCD_pixel,registered_bin_cloud{1,i}(:,2)*CCD_pixel,registered_bin_cloud{1,i}(:,3)*CCD_pixel,1,color(ceil(100/Number_of_bins*i),:),'.'),title(['bin ' num2str(i)]);
        sgtitle('Reconstruction per bin')
        hold on
        xlim([-100 100])
        ylim([-100 100])
        zlim([-100 100])
    end
figure;    
% Plots using render projection
    for i=1:Number_of_bins
        subplot(2,3,i)
        renderprojection(registered_bin_cloud{1,i}(:,1)*130,registered_bin_cloud{1,i}(:,3)*130,zeros(length(registered_bin_cloud{1,i}),1),180,0, [-160 160],[-160 160],1,1, 1, .1),title(['bin ' num2str(i)]);
        sgtitle('Reconstruction per bin (xz side view)')
        axis off
        hold on
        plot(  [260; 300], [300; 300], '-w', 'LineWidth', 4)
        text(280,280, '40 nm','color','w', 'HorizontalAlignment','center')
        axis off

    end

        

    
    