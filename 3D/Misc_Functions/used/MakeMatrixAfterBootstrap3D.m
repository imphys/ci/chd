function [MatrixAfterBootstrap,MatrixAfterBootstrap_norm] = MakeMatrixAfterBootstrap3D(subParticles)
    
    K = length(subParticles); 
    MatrixAfterBootstrap = zeros(K,K); 
    
    %make registration matrix
    for m = 1:K-1
        for s = m+1:K
%             M_resampled = resampleCloud2D(particlesAligned{m});
%             S_resampled = resampleCloud2D(particlesAligned{s}); 
%             MatrixAfterBootstrap(m,s) = mex_expdist(M_resampled.points, S_resampled.points, M_resampled.sigma, S_resampled.sigma);
            MatrixAfterBootstrap(m,s) = mex_expdist(subParticles{s}.points, subParticles{m}.points, correct_uncer(subParticles{s}.sigma), subParticles{m}.sigma,eye(3));
        end
    end

    %Normalize 
    numLocs = cellfun(@(v) length(v.points), subParticles);

    MatrixAfterBootstrap_norm = MatrixAfterBootstrap;
    for i = 1:K
        for j = 1:K
            if MatrixAfterBootstrap(i,j)>0
                MatrixAfterBootstrap_norm(i,j)=MatrixAfterBootstrap(i,j)/(numLocs(i)*numLocs(j));       %Na*Nb
            end
        end
    end

end

