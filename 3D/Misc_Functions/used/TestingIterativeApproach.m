clc
% close all
clear clusters_it win
tic
% A = 1:100;
% B = setdiff(1:K,A); 
% random{1} = A';
% random{2} = B';
% clusters_it{1} = random; 

clusters_it{1} = clusters2; 
% inClusters1 = (vertcat(clusters1{:})); 
% 
% extracted_bootstrapped_extended = extracted_bootstrapped; 
% for i = setdiff(1:K,inClusters1)
%     putIn = subParticles{i}.points; 
%     extracted_bootstrapped_extended = {extracted_bootstrapped_extended{1:i-1} {putIn} extracted_bootstrapped_extended{i:end}};
% end
% parts = extracted_bootstrapped_extended;
% super = vertcat(parts{:}); 
% super = vertcat(super{:});
stop = false; 

figure()
plot(clusters_it{1}{1},zeros(1,length(clusters_it{1}{1})),'o')
hold on
plot(clusters_it{1}{2},ones(1,length(clusters_it{1}{2})),'o')
title('1')

%ITM
scale = [0.1]; 
for i = 2:4
    if stop==false
        [out1,out2,out3,out4] = OneIterativeRound3D(subParticles,superParticleSymm,clusters_it{i-1},i,scale);       %{super}/superParticle
        clusters_it{i} = out1;
        win{i} = out2;   
        
        if (isequal(clusters_it{i-1}, clusters_it{i})  ||  isempty(clusters_it{i}{1}) || isempty(clusters_it{i}{2}))
            stop = true;
        end
        
    end
end

for i = 2:length(win)
    figure()
    plot(win{i},'o')
    title(num2str(i))
end


for it = 1:length(clusters_it)
    disp(['it ' num2str(it) repmat('     ',1,it) ': ' num2str(sum(clusters_it{it}{1}<(threshold+1))) ' normal ' num2str(sum(clusters_it{it}{1}>(threshold)))  ' mirror'] )
    disp(['it ' num2str(it) repmat('     ',1,it) ': ' num2str(sum(clusters_it{it}{2}<(threshold+1))) ' normal ' num2str(sum(clusters_it{it}{2}>(threshold)))  ' mirror'] )
    wrong{it} = sort( [ clusters_it{it}{2}((clusters_it{it}{2}>threshold));  clusters_it{it}{1}((clusters_it{it}{1}<(threshold+1))) ]);
end
toc

for i = 1:length(clusters_it)
    [mean(all_shifts(clusters_it{i}{1})) std(all_shifts(clusters_it{i}{1}))   mean(all_shifts(clusters_it{i}{2})) std(all_shifts(clusters_it{i}{2}))   ]
end

 %% random plots 
% figure()
% dif = abs(out3(:,1)-out3(:,2));
% plot(dif,'ro')A = 1:100;
B = setdiff(1:K,A); 
random{1} = A';
random{2} = B';
clusters_it{1} = random; 
% hold on
% plot(wrongx,dif(wrongx'),'bo')
% hold on
% pr = prctile(dif,20); 
% plot([0 K],[pr pr])
% 
% redo_ind = find(dif<pr);
% redone = [abs(out5(:,1)>out5(:,2)) abs(out5(:,3)>out5(:,4)) abs(out5(:,5)>out5(:,6)) abs(out5(:,7)>out5(:,8))];
% 
% shoh = [redo_ind win{2}(redo_ind)' redone] 
% 
% 
% figure()
% plot(out3,'.-')
% 
% len = cellfun(@(v) size(v.points,1), subParticles);  
% 
% figure()
% plot(len,out3(:,2),'bo')
% title('#locs vs. costValue')
% 
% figure()
% plot(cA,out3(cA,1)./len(cA)','ro')
% hold on
% plot(cB,out3(cB,1)./len(cB)','bo')
% title('1')
% hold on
% 
% 
% cA = clusters_it{2}{1}; 
% cB = clusters_it{2}{2}; 
% 
% distrA1 = [ mean(out3(cA,1)./len(cA)')  std(out3(cA,1)./len(cA)') ]
% distrB2 = [ mean(out3(cB,2)./len(cB)')  std(out3(cB,2)./len(cB)') ]
% distrA2 = [ mean(out3(cA,2)./len(cA)')  std(out3(cA,2)./len(cA)') ]
% distrB1 = [ mean(out3(cB,1)./len(cB)')  std(out3(cB,1)./len(cB)') ]
% 
% pA = normpdf(out3(:,1)./len',distrA(1), distrA(2)); 
% pB = normpdf(out3(:,2)./len',distrB(1), distrB(2)); 
% figure()
% plot(pA>pB,'o')
% % plot(pA,'r.')
% % hold on
% % plot(pB,'b.')
% test = [pA pB pA>pB]
% % [mean(out3(1:threshold,2)./len(1:threshold)')  std(out3(1:threshold,2)./len(1:threshold)')]
% % [mean(out3(threshold+1:end,1)./len(threshold+1:end)')  std(out3(threshold+1:end,1)./len(threshold+1:end)')]
% 
% test = extractFromSuperParticle(subParticles,superParticle(5),clusters_it{2});
% visualizeCloud2D(test{1}{1},400,width,0,'1')
% visualizeCloud2D(test{2}{1},400,width,0,'2')
% 
% overlayImage(test,1.5,400); 
% 
% %% Visualize...
% 
% %visualize 
% visualizeCloud2D(template1,400,width,0, 'template1');
% visualizeCloud2D(template2,400,width,0, 'template2');
% visualizeCloud2D(MA.points,400,width,0, 'template1 down');
% visualizeCloud2D(MB.points,400,width,0, 'template2 down');
% visualizeCloud2D(subParticles{27}.points,400,width,0, '21');
% 
% visualizeCloud2D(regA.points,400,width,0, 'reg1');
% visualizeCloud2D(regB.points,400,width,0, 'reg2');
% visualizeCloud3D_Teun(MA.points,200,width,0, 'MA');
% visualizeCloud3D_Teun(MB.points,200,width,0, 'MB');

visualizeCloud3D(MA.points,0.05,1,'MA');
visualizeCloud3D(MB.points,0.05,1,'MB');

% 
% visualizeCloud2D(par_transA,400,width,0, 'par transA');
% visualizeCloud2D(par_transB,400,width,0, 'par transB');
% visualizeCloud2D(subParticles2{199}.points,400,width,0, '199filt');
% 
% %% reboot final output
% clc
% 
% iter = 1; 
% [~,superParticle_result] = reBootstrap(extracted,subParticles,clusters_it{end},iter,'notsym');  
% 
% visualizeCloud2D(superParticle_result{1},200,width,0, ['class1 result']);
% visualizeCloud2D(superParticle_result{2},200,width,0, ['class2 result']);
% 
 %% Redo pairFitting some particles 
clc
MA = out4{1}; 
MB = out4{2}; 

clear l
clc
scale = [0.1]; 
output = []; 

for i = 1%wrong{2}(1:4)'-10
    par = subParticles_slices{39}; 
%     [parA, regA, history, config, valTestA, outA] = pairFitting(par, MA, scale,18);
%     [parB, regB, ~, ~, valTestB, outB] = pairFitting(par, MB, scale,18);

     [parA, valTestA] = pairFitting3D(par.points, MA.points, ...
                               par.sigma, MA.sigma, scale, 1, 1, 1,16);
     [parB, valTestB] = pairFitting3D(par.points, MB.points, ...
                              par.sigma, MB.sigma, scale, 1, 1, 1,16);
        l(i) = (valTestA>valTestB)

end
parA
%% Show templates + registered particles 
% 
% % parA = outA{3}{1}; 
% 
par_transA = transform_by_rigid3d(par.points,parA); 
par_transB = transform_by_rigid3d(par.points,parB); 
figure()
scatter3(MA.points(:,1),MA.points(:,2),MA.points(:,3),5,'.')
hold on
scatter3(par_transA(:,1), par_transA(:,2), par_transA(:,3),100,'r.')
% xlim([-0.3 0.3])
% ylim([-0.3 0.3])
title(['MA: ' num2str(valTestA)])
axis equal

figure()
scatter3(MB.points(:,1),MB.points(:,2),MB.points(:,3),5,'.')
hold on
scatter3(par_transB(:,1), par_transB(:,2),par_transB(:,3),100,'r.')
title(['MB: ' num2str(valTestB)])
axis equal

figure()
scatter3(par_transB(:,1), par_transB(:,2),par_transB(:,3),100,'r.')
axis equal
% 
% figure()
% scatter(MA.points(:,1),-MA.points(:,2),'.')
% title('MA')
% figure()
% scatter(MB.points(:,1),-MB.points(:,2),'.')
% title('MB')
% 
% %cost value of angles A
% figure()
% plot(outA{2}(1:end),outA{1},'o-','MarkerFaceColor','auto')
% hold on
% plot(outA{2}(1:2:end),outA{1}(1:2:end),'o','MarkerFaceColor','k')
% % hold on
% % plot(outA{2}(2:2:end),outA{1}(2:2:end),'o','MarkerFaceColor','r')
% xlabel('initialized angle')
% ylabel('found costValue')
% title('Cost-function value for each initialized angle')
% grid on
% xlim([-pi pi])
% 
% %cost value of angles B
% figure()
% plot(outB{2}(1:end),outB{1},'o-','MarkerFaceColor','auto')
% hold on
% plot(outB{2}(1:2:end),outB{1}(1:2:end),'o','MarkerFaceColor','k')
% % hold on
% % plot(outA{2}(2:2:end),outA{1}(2:2:end),'o','MarkerFaceColor','r')
% xlabel('initialized angle')
% ylabel('found costValue')
% title('Cost-function value for each initialized angle')
% grid on
% xlim([-pi pi])
% 
% %input vs. output angle 
% figure()
% plot(outA{2},cellfun(@(v) v(3),outA{3}),'o-')
% hold on
% plot(outA{2}(1:6:end),cellfun(@(v) v(3),outA{3}(1:6:end)),'o','MarkerFaceColor','k')
% hold on
% plot(outA{2},outA{2})
% xlabel('initialized angle')
% ylabel('found angle')
% title('Found angle for each initialized angle')
% grid on
% xlim([-pi pi])
% 
% figure()
% plot(outA{2},cellfun(@(v) v(1),outA{3}),'.-')
% title('dx')
% grid minor
% grid
% figure()
% plot(outA{2},cellfun(@(v) v(2),outA{3}),'.-')
% title('dy')
% grid minor 
% grid
% 
%% Manual cost function
% clear output output2 dist dist2 val testDistA testDistB
% clc
% 
% for s = 1
%     %sc = 10^(-s);
%     dist = 0;
%     dist2 = 0; 
%     for i = 1:size(par_transA,1)
%         for j = 1:size(MA.points,1)
%                 val = exp(  -sum((par_transA(i,:)-MA.points(j,:)).^2) / (2*( par.sigma(i)+MA.sigma(j) )) ) / ( par.sigma(i)+MA.sigma(j) ) ; 
%                 dist =    dist   + val ;
%                 testDistA(i,j) = val; 
%         end
%         for j = 1:size(MB.points,1)
%                 val = exp(  -sum((par_transB(i,:)-MB.points(j,:)).^2) / (2*( par.sigma(i)+MB.sigma(j) )) ) / ( par.sigma(i)+MB.sigma(j) ) ; 
%                 dist2 =  dist2 + val;
%                 testDistB(i,j) = val; 
%         end
%     end
%     output(s) = dist;
%     output2(s) = dist2;
% 
% end
% output
% output2
%     
% 
% figure()
% temp1 = sum(testDistA,2); 
% plot(temp1,'.')
% 
% % figure()
% hold on
% temp2 = sum(testDistB,2); 
% plot(temp2,'r.')
% 
%% high-sigma investigation
% 
% for i = 1:length(subParticles)
%         sigmas{i} = subParticles{i}.sigma;
% end
% allSigmas = vertcat(sigmas{:});
% allSigmasNM = sqrt(allSigmas)*130; 
% 
%  
% figure()
% %  thr = prctile(allSigmasNM,90); 
% %  thr = max(allSigmasNM); 
% histogram(allSigmasNM(allSigmasNM<2),1000,'EdgeColor','auto','FaceAlpha',1)%,'Normalization','pdf')
% title('below 2 ')
% xlabel('uncertainty (nm)')
% ylabel('count')
% xlim([0 2])
% title('Localization uncertainty distribution')
%  
% mean(allSigmasNM)
% mean(allSigmasNM(allSigmasNM<2))
% 
%% filter sigmas
%NOT NECESSARY, BECAUSE ALREADY FILTERED BEFORE RECONSTRUCTION!!
clear take takeTotal subParticles2

takeTotal = [];

for i = 1:length(subParticles)
    take = subParticles{i}.sigma <((2/130)^2);
    takeTotal = [takeTotal; take];
    subParticles2{i}.points = subParticles{i}.points(take,:); 
    subParticles2{i}.sigma = subParticles{i}.sigma(take); 
end


% super2 = super(logical(takeTotal),:);

for i = 1:5
    superParticle2{i} = superParticle{i}(logical(takeTotal),:); 
end
%% Manual rotation + costVal
% tic
% parCentred = par;
% parCentred.points(:,1) = par.points(:,1)-mean(par.points(:,1)); 
% parCentred.points(:,2) = par.points(:,2)-mean(par.points(:,2)); 
% 
% % dx = cellfun(@(v) v(1), outA{3});
% % dy = cellfun(@(v) v(2), outA{3});
% % da = cellfun(@(v) v(3), outA{3});
% 
% clear angles rot parCent_rot output output2
% 
% angles = linspace(-pi,pi,360); 
% for i = 1:length(angles)
%     rot = [0, 0, angles(i)];
%     parCent_rot = transform_by_rigid2d(parCentred.points,rot); 
%     
%     A.points = parCent_rot;
%     A.sigma = par.sigma; 
%     
%     output(i) = mex_expdist(A.points, MA.points, A.sigma, MA.sigma); 
%     
% %     figure()
% %     scatter(MA.points(:,1),-MA.points(:,2),'.')
% %     hold on
% %     scatter(parCent_rot(:,1),-parCent_rot(:,2),'r.')    
% %     title(num2str(angles(i)))
% %     xlim([-0.3 0.3])
% %     ylim([-0.3 0.3])
% %     axis square
% end
% 
% figure
% plot(angles,output,'.-')
% xlabel('rotated angle')
% ylabel('cost function')
% xlim([-pi pi])
% grid on
% toc
% 
%% Plot moving history of optimization
% clc
% his = history{25}.x; 
% 
% f=figure('Color','w');
% fileName = 'history26.gif';
% 
% for i = 1:size(his,1)
% clf(f)
% rot = his(i,:); 
% par_transA = transform_by_rigid2d(par.points,rot); 
% 
% scatter(MA.points(:,1),-MA.points(:,2),'.')
% hold on
% scatter(par_transA(:,1), -par_transA(:,2),'r.')
% axis square
% xlim([-0.3 0.3])
% ylim([-0.3 0.3])
% title([num2str(i) ' / ' num2str(size(his,1))])
% pause(0.2)
%      
% %       frame = getframe(f); 
% %       im = frame2im(frame); 
% %       [imind,cm] = rgb2ind(im,256); 
% %       % Write to the GIF File 
% %       if i== 1 
% %           imwrite(imind,cm,fileName,'gif', 'Loopcount',inf); 
% %       else 
% %           imwrite(imind,cm,fileName,'gif','WriteMode','append'); 
% %       end 
% end
% 
%%
% figure()
% for i = 24:26%1:length(history)
%     his = history{i}.x; 
%     plot3(his(1,1),his(1,2),his(1,3),'o','MarkerFaceColor','k')
%     hold on
%      plot3(his(end,1),his(end,2),his(end,3),'o','MarkerFaceColor','k')
%     hold on
%     plot3(his(:,1),his(:,2),his(:,3),'.-')
%     hold on
% end
% grid
% xlabel('x position')
% ylabel('y position')
% zlabel('angle')
% title('all initializations')
% 
% 
% %% Manders
% 
% ratio = size(MA.points,1)/size(MB.points,1);        %~37/30
% 
% M1 = out3;          % div red
% 
% M2 = out3;          % div blue 
% M2(:,2) = M2(:,2)*ratio; 
% 
% M1_diff = abs(M1(:,1)-M1(:,2))./mean(M1,2); 
% M2_diff = abs(M2(:,1)-M2(:,2))./mean(M2,2); 
% 
% output = M1_diff>M2_diff; 
% 
% 
% figure()
% plot(M1_diff)
% hold on
% plot(M2_diff)
% title('M1 and M2 diff')
% 
% 
% figure()
% plot(output,'o')
% title('output')
% 
% figure 
% plot(out3(:,1))
% hold on
% plot(out3(:,2)*ratio)
% title('out3 ratio')
% 
% 
% 

%% make resulting particles 
% A = find(all_shifts<median(all_shifts)); 
% B = setdiff(1:K,A); 
% random{1} = A';
% random{2} = B';

random = clusters_it{2};
test = extractFromSuperParticle(subParticles,superParticleSymm(end),random);



%% ITM for remap8Fold

%convert subParticles to complete rings 
len = cellfun(@(v) size(v.points,1), subParticles); 
len_all = sum(len); 
clear subParticles_slices

%subParticles_slices
for i = 1:K
    subParticles_slices{i}.points = [];
    for j = 1:8
        x= {superParticleSymmComplete{end}((1:len_all)+(j-1)*len_all,:)};
        temp = extractFromSuperParticle(subParticles,x , {i}); 
        subParticles_slices{i}.points = [subParticles_slices{i}.points; temp{1}{1}];
    end
    subParticles_slices{i}.sigma = repmat(subParticles{i}.sigma,8,1); 
end

%superParticle Complete
MA.points = []; 
MA.sigma = [];
% iA =  [1:50 101:150];
% iB = [51:100 151:200];
% iA = sort(randperm(200,100)); 
% iB = setdiff(1:K,iA); 
% iA = find(win==1); 
% iB = find(win==0); 
iA = clusters2{1}'; 
iB = clusters2{2}'; 

for i =iA
    MA.points = [MA.points; subParticles_slices{i}.points];
    MA.sigma = [MA.sigma; subParticles_slices{i}.sigma];
end
MB.points = []; 
MB.sigma = [];
for i = iB
    MB.points = [MB.points; subParticles_slices{i}.points];
    MB.sigma = [MB.sigma; subParticles_slices{i}.sigma];
end

ids1 = randperm(size(MA.points,1),5000);
ids2 = randperm(size(MB.points,1),5000);
MA.points = MA.points(ids1,:); 
MA.sigma = MA.sigma(ids1,:); 
MB.points = MB.points(ids2,:); 
MB.sigma = MB.sigma(ids2,:); 

% visualizeCloud3DSurface_Teun(MA.points,100)
% visualizeCloud3DSurface_Teun(MB.points,100)
% 
% [MA.points, ids1] = resampleCloud3D_Teun(MA.points,0.6,5000);
% MA.sigma = MA.sigma(ids1,:); 
% [MB.points, ids2] = resampleCloud3D_Teun(MB.points,0.6,5000);
% MB.sigma = MB.sigma(ids1,:); 
% 
% visualizeCloud3DSurface_Teun(MA.points,100)
% visualizeCloud3DSurface_Teun(MB.points,100)
% visualizeCloud3DSurface_Teun(subParticles_slices{132}.points,100)



clear win
scale = [0.1]; 
output = []; 
% I = zeros(1,K)+2; 

parfor i = 1:K
    disp(['started: ' num2str(i)])
    par = subParticles_slices{i}; 
%     [parA, regA, history, config, valTestA, outA] = pairFitting(par, MA, scale,18);
%     [parB, regB, ~, ~, valTestB, outB] = pairFitting(par, MB, scale,18);

    [parA, valTestA] = pairFitting3D(par.points, MA.points, ...
                               par.sigma, MA.sigma, scale, 1, 1, 1,16);
    [parB, valTestB] = pairFitting3D(par.points, MB.points, ...
                              par.sigma, MB.sigma, scale, 1, 1, 1,16);
    win(i) = (valTestA>valTestB)
    output(i,:) = [valTestA valTestB]
     
end
disp('done')



figure()
plot(win,'o')

threshold = 250; 
for it = 1
    disp(['it ' num2str(it) repmat('     ',1,it) ': ' num2str(sum(find(win==0)<(threshold+1))) ' normal ' num2str(sum(find(win==0)>(threshold)))  ' mirror'] )
    disp(['it ' num2str(it) repmat('     ',1,it) ': ' num2str(sum(find(win==1)<(threshold+1))) ' normal ' num2str(sum(find(win==1)>(threshold)))  ' mirror'] )
    wrong = sort( [  find(win(1:threshold)==0) find(win(threshold+1:end)==1)+threshold]);
end

figure()
plot(abs(output(:,1)-output(:,2)),'o')
hold on
plot(wrong,abs(output(wrong,1)-output(wrong,2)),'ro')

%% repeat 1 specific


for i = 40
    disp(['started: ' num2str(i)])
    par = subParticles_slices{i}; 
%     [parA, regA, history, config, valTestA, outA] = pairFitting(par, MA, scale,18);
%     [parB, regB, ~, ~, valTestB, outB] = pairFitting(par, MB, scale,18);

     [parA, valTestA] = pairFitting3D(par.points, MA.points, ...
                               par.sigma, MA.sigma, scale, 1, 1, 1,16);
     [parB, valTestB] = pairFitting3D(par.points, MB.points, ...
                              par.sigma, MB.sigma, scale, 1, 1, 1,16);
     [i valTestA>valTestB]
     
end
disp('done')



par_transA = transform_by_rigid3d(par.points,parA); 
par_transB = transform_by_rigid3d(par.points,parB); 
figure()
scatter3(MA.points(:,1),MA.points(:,2),MA.points(:,3),5,'.')
hold on
scatter3(par_transA(:,1), par_transA(:,2), par_transA(:,3),100,'r.')
% xlim([-0.3 0.3])
% ylim([-0.3 0.3])
title(['MA: ' num2str(valTestA)])
axis equal

figure()
scatter3(MB.points(:,1),MB.points(:,2),MB.points(:,3),5,'.')
hold on
scatter3(par_transB(:,1), par_transB(:,2),par_transB(:,3),100,'r.')
title(['MB: ' num2str(valTestB)])
axis equal






