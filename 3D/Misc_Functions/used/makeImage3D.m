function [Z] = makeImage3D(X, n, diameter)
    
    % extract the ROI
    ROIradius = 0.5*diameter;
    X = X(X(:,1) < ROIradius & X(:,1) > (-ROIradius),:);
    X = X(X(:,2) < ROIradius & X(:,2) > (-ROIradius),:);
    X = X(X(:,3) < ROIradius & X(:,3) > (-ROIradius),:);

    % define the grid
    xi = linspace(-ROIradius,ROIradius,n);
    yi = linspace(-ROIradius,ROIradius,n);
    zi = linspace(-ROIradius,ROIradius,n);

    % discretize the particle coordinates
    xr = interp1(xi,1:numel(xi),X(:,1),'nearest');
    yr = interp1(yi,1:numel(yi),X(:,2),'nearest');
    zr = interp1(zi,1:numel(zi),X(:,3),'nearest');

    % binning
    Z = accumarray([xr yr zr],1, [n n n]);
    
end