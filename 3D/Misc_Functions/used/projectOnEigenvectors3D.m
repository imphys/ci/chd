function [weights2,KM2,clusters,x_all] = projectOnEigenvectors3D(parts, eigenvectors, width)
   
    N = round((size(eigenvectors,1))^(1/3));
    
    for i = 1:length(parts)
        I = parts(i);
        I = makeImage3D(I{1}{1},N,width);
        I = imgaussfilt3(I,N/30);   
        I = I-mean(I(:)); 
        I = I/norm(I(:)); 

        Im_all{i} = I;     
        x_all(:,i) = reshape(Im_all{i},N^3,1);

    end
    
    x_mean = mean(x_all,2);
    x_all =x_all-x_mean;

    weights2 = x_all'*eigenvectors;
    KM2 = kmeans(weights2(:,1),2,'Replicates',10);
    
    for c = 1:max(KM2)
        clusters{c} = find(KM2==c);
    end

end