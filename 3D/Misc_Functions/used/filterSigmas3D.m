function [subParticles2] = filterSigmas3D(subParticles,prc)

    for i = 1:length(subParticles)
            sigmas{i} = subParticles{i}.sigma(:,1);
    end
    allSigmas = vertcat(sigmas{:});
    
    threshold = prctile(allSigmas,prc); 
    thresholdNM = sqrt(threshold)*130;
    
    for i = 1:length(subParticles)
        take = subParticles{i}.sigma(:,1) <threshold;      %2nm 
        subParticles2{i}.points = subParticles{i}.points(take,:); 
        subParticles2{i}.sigma = subParticles{i}.sigma(take,:); 
    end
end