function [image, profile] = getCircleImageProfile(points,N)

%project in z 
block = makeImage3D(points,N,1.5);
image = sum(block,3); 

%determine radius 
[x,y] = meshgrid(linspace(-N/2,N/2,N), linspace(-N/2,N/2,N)); 
r = sqrt(x.^2 + y.^2); 
weightedR = r.*image; 
rcentre = sum(weightedR(:)) / sum(image(:));

theta = linspace(0,2*pi,1000); 
x = rcentre*cos(theta)+N/2; 
y = rcentre*sin(theta)+N/2; 
c = improfile(image,x,y,1000);

profile.x = x;
profile.y = y; 
profile.theta = theta;
profile.c = c; 
profile.data = [theta' c];

end

