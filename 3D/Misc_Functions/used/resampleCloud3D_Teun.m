% resampleCloud2D   subsamples the particle based on density of
% localizations
%
% SYNOPSIS:
%   [NewParticle] = resampleCloud2D(Particle)
%
% INPUT
%   Particle
%       The particle to be downsampled.
%
% NewParticle
%       The downsampled particle.
%
% NOTES
%       The weights for downsampling are computed using the intensity of 
%       the image resulting from binning and smoothing the localizations.
%
% (C) Copyright 2017               Quantitative Imaging Group
%     All rights reserved          Faculty of Applied Physics
%                                  Delft University of Technology
%                                  Lorentzweg 1
%                                  2628 CJ Delft
%                                  The Netherlands
%
% Hamidreza Heydarian, 2017

function [ NewParticle,ids_original,weights] = resampleCloud3D_Teun(Particle,width,cutoff)

%     meansigma = mean((Particle.sigma));         % the average uncertainties
    meansigma = 0.0001;
    S = size(Particle,1);                % particles sizefitCircleDownsample
%     cutoff = 5000;                              % the max number of 
                                                % localizations to be kept

    % binning the localizations
%     xmax=max(Particle(:,1));
%     xmin=min(Particle(:,1));
%     ymax=max(Particle(:,2));
%     ymin=min(Particle(:,2));

    wrongX = find(abs(Particle(:,1))>width);
    wrongY = find(abs(Particle(:,2))>width);
    wrongZ = find(abs(Particle(:,3))>width);
    wrong = sort(unique([wrongX; wrongY; wrongZ]));
    good = setdiff(1:S,wrong); 
    
    S = length(good);
    Particle = Particle(good,:);
    
    xmax = width; 
    xmin = -width; 
    ymax = width; 
    ymin = -width;
    zmax = width; 
    zmin = -width;
    
    dmax = [xmax ymax zmax];
    dmin = [xmin ymin zmin];
    nn = 100;                                   % number of bins
    bins = [1 1 1].*nn;
    binsize = (dmax - dmin)./bins;             % bin size

    xi = linspace(dmin(1),dmax(1),bins(1));
    yi = linspace(dmin(2),dmax(2),bins(2));
    zi = linspace(dmin(3),dmax(3),bins(3));

    xr = interp1(xi,1:numel(xi),Particle(:,1),'nearest');
    yr = interp1(yi,1:numel(yi),Particle(:,2),'nearest');
    zr = interp1(zi,1:numel(zi),Particle(:,3),'nearest');

    subs = [xr yr zr];

    % act like addgaussianblob
    fsize = double(meansigma./binsize(1));  % filter size
    
    % smoothe the image
    f = fspecial3('gaus',[round(7*fsize)+1 round(7*fsize)+1 round(7*fsize)+1],fsize);
    
    % localizations is discretized to computed the weights for resampling
    binned = accumarray(subs,1, bins);          % discretized image
    localdensity = imfilter(binned,f,'same');    % smoothed image

    % weights for resampling function
    weights = zeros(1,S);
    for l = 1:S
        weights(1,l) = localdensity(xr(l),yr(l),zr(l));
    end
    
    % make sure that there is no NAN or negative weights
    weights(isnan(weights)) = 0;
    weights = max(weights,0);
    Max = numel(weights(weights > 0));

    % perform the weighted resampling
    ids = datasample(1:S,min(Max,cutoff),'Replace',false,'Weights',weights);

    % new particle
    NewParticle = Particle(ids,:);
%     NewParticle.sigma = Particle.sigma(ids,:);
    ids_original = good(ids);   %because points far away are filtered out 

end