function [clusters2, win, outputValues, templates] = OneIterativeRound3D(subParticles, superParticle,clusters1,iter,scale)

    len = cellfun(@(v) size(v.points,1), subParticles);  
    downSamp = round(mean(len)); 
    downSamp = 5000; 
    
    disp(['downsampled to: ' num2str(downSamp)]); 
    
    [MA,MB] = makeTemplates3D(subParticles,superParticle,clusters1,downSamp);
    templates = {MA,MB};

   
    
    parfor i = 1:length(subParticles)
        
        disp([num2str(iter) ' ' num2str(i)])

%         S = subParticles{i}; 

%         [parA, ~, ~, ~, valPairA1] = pairFitting(S, MA, scale,18);
%         [parB, ~, ~, ~, valPairB1] = pairFitting(S, MB, scale,18);
        
        [parA, valPairA1] = pairFitting3D(subParticles{i}.points, MA.points, ...
                               subParticles{i}.sigma, MA.sigma, scale, 1, 1, 1,16);
        [parB, valPairB1] = pairFitting3D(subParticles{i}.points, MB.points, ...
                               subParticles{i}.sigma, MB.sigma, scale, 1, 1, 1,16);

        win(i) = valPairA1<valPairB1;
        
%         valPairA2 = evaluateWithCustomSigma(S,parA,MA,1e-4);
%         valPairB2 = evaluateWithCustomSigma(S,parB,MB,1e-4);       
        
        outputValues(i,:) = [valPairA1,valPairB1];
        
    end
    
    disp('done')
    
    c_1 = mat2cell(find(win==0)',sum(win==0));
    c_2 = mat2cell(find(win==1)',sum(win==1));

    clusters2{1} = c_1{:};
    clusters2{2} = c_2{:};
    
end




