function [MatrixAfterAll2all, MatrixAfterAll2all_norm] = MakeMatrix3D(Values, I_all2all,subParticles)

K = length(subParticles);
Mat = zeros(K,K);
Mat_norm = zeros(K,K);

numLocs = cellfun(@(v) size(v.points,1),subParticles);

for i = 1:length(Values)
    indx = I_all2all(:,i);
    Mat(indx(1),indx(2)) = Values(i); 
    Mat_norm(indx(1),indx(2)) = Values(i)/(numLocs(indx(1))*numLocs(indx(2)));
end

MatrixAfterAll2all = Mat; 
MatrixAfterAll2all_norm = Mat_norm; 


end

