function [MA,MB] = makeTemplates3D(subParticles,superParticle,clusters1,downSamp)

    superParticle_class_fromSP1 = extractFromSuperParticle(subParticles,superParticle,clusters1);    
    template1 = superParticle_class_fromSP1{1}{end};
    template2 = superParticle_class_fromSP1{2}{end}; 
    
    template1_locPerPar = size(template1,1)/length(clusters1{1});       %#localizations per particle
    template2_locPerPar = size(template2,1)/length(clusters1{2});       

    downSamp = min([downSamp size(template1,1) size(template2,1)]);     %downSamp to less than 5000 if one template has less localizations 
    
    downSamp1 = round(downSamp * template1_locPerPar / max([template1_locPerPar template2_locPerPar]));        %scale downSamp to number of locs per particle, necessary when one class has less binding sites (w/o flame dataset) 
    downSamp2 = round(downSamp * template2_locPerPar / max([template1_locPerPar template2_locPerPar])); 

%     [template1_down, ids1] = resampleCloud2D_Teun(template1,0.6,downSamp1); 
%     [template2_down, ids2] = resampleCloud2D_Teun(template2,0.6,downSamp2);
%     
    ids1 = randperm(size(template1,1),downSamp1);
    ids2 = randperm(size(template2,1),downSamp2); 
    template1_down = template1(ids1,:); 
    template2_down = template2(ids2,:); 
    
    for i = 1:length(subParticles)
        sigmas{i} = subParticles{i}.sigma;
    end

    sigmas1 = vertcat(sigmas{clusters1{1}});
    sigmas1 = sigmas1(ids1,:);
    sigmas2 = vertcat(sigmas{clusters1{2}});
    sigmas2 = sigmas2(ids2,:);

    MA.points = template1_down;
    MA.sigma = sigmas1;
    MB.points = template2_down;
    MB.sigma = sigmas2;

end