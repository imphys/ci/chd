function [R] = rotationAroundArbAxis(axis,angle)

ux = axis(1); 
uy = axis(2); 
uz = axis(3);
cth = cos(angle); 
sth = sin(angle); 

R = [ cth+ux^2*(1-cth)          ux*uy*(1-cth)-uz*sth     ux*uz*(1-cth)+uy*sth;
     uy*ux*(1-cth)+uz*sth       cth+uy^2*(1-cth)        uy*uz*(1-cth)-ux*sth;
     uz*ux*(1-cth)-uy*sth        uz*uy*(1-cth)+ux*sth    cth+uz^2*(1-cth) ];
 
end

