function [eigenvectors, eigenvalues, x_mean, weights, Im] = eigenAnalysis3D(width, clusters2, superParticle_class_fromSP )

    N=100; 
    C = length(clusters2); 

    iter = length(superParticle_class_fromSP{1});

    % convert point clouds to images 
    for i = 1:C
        I = superParticle_class_fromSP{i}{iter}; 
    %     I = resampleCloud2D_Teun(I,numDownSamp); 
        I = makeImage3D(I,N,width);
        I = imgaussfilt3(I,N/30);   
        I = I-mean(I(:)); 
        I = I/norm(I(:)); 

        Im{i} = I;     
        x(:,i) = reshape(Im{i},N^3,1);

    end

    x_mean = mean(x,2);
    x = x-x_mean; 
    Cov = x'*x; 
    [U,S,~] = svd(Cov);   %A = USV'; 
       % U: columns are vectors 
       % S: diagonal are eigenvalues  
       % V: columns are vectors 

    eigenvalues = diag(S);
    eigenvectors = x*U;
    for i = 1:C
        eigenvectors(:,i) = eigenvectors(:,i)/norm(eigenvectors(:,i),2); 
    end
    weights = x'*eigenvectors; 

end


