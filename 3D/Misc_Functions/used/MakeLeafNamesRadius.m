function [names] = MakeLeafNamesRadius(order,all_rs)

    names = cellfun(@(v) insertBefore(v,v,'Image '),strsplit(num2str(order)),'UniformOutput',false);
    for i = 1:length(order)
        if all_rs(i)>median(all_rs)
%             names = regexprep(names,['Image ' num2str(i)],; 
            names{i} = ['- ' num2str(order(i))];
        end
    end
end

