function [superParticle_class_fromSP] = extractFromSuperParticle(subParticles,superParticle,clusters2)

    numLocs = cellfun(@(v) length(v.points), subParticles);
    
    for iter = 1:length(superParticle)
        for c = 1:length(clusters2) 
            members = sort(clusters2{c});
            par = [];
            for i = 1:length(members)
                memberi = members(i);
                start = sum(numLocs(1:(memberi-1)))+1;
                stop = sum(numLocs(1:memberi)); 
                par = [par; superParticle{iter}(start:stop,:)];
            end
            superParticle_class_fromSP{c}{iter} = par;
        end
    end
end

