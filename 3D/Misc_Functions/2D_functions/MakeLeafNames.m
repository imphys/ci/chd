function [names] = MakeLeafNames(order,threshold)
    names = cellfun(@(v) insertBefore(v,v,'Image '),strsplit(num2str(order)),'UniformOutput',false);
    for i = 1:length(order)
        if i>threshold%(length(order)/2)
%             names = regexprep(names,['Image ' num2str(i)],['- ' num2str(i)]); 
                        names{i} = ['- ' num2str(order(i))];
        end
    end
end

