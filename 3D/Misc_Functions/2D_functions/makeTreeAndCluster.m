function [clusters] = makeTreeAndCluster(mat, distanceMethod,names,numclust,print_flag)

    mat_full = mat+mat';
    distanceMat = max(mat_full(:)) - mat_full;  
    Tree = seqlinkage(distanceMat,distanceMethod,names);

    distances = get(Tree,'distances');
    pointers = get(Tree,'Pointers');
    nodeNames = get(Tree,'NodeNames');
    BranchLeafNumber = str2double(extractAfter(nodeNames,' '));

    [clustered, branches_clustered] = cluster(Tree,[],'maxclust',numclust);
    
    if print_flag
        h = plot(Tree,'Type','square');
        title(distanceMethod)

        %color branches 
        colors = hsv(length(unique(branches_clustered)));  %jet, hsv, lines,colorcube
        for b = 1:length(unique(branches_clustered))
            set(h.BranchLines(branches_clustered==b),'Color',colors(b,:))
        end
    end

    for i = 1:max(clustered) 
        clusters{i} = BranchLeafNumber((clustered==i));
        clusters{i} = sort(clusters{i}); 
    end

end

