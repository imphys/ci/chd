function [p1] = visualizeMatrix3DSurface(Z,quarants,v1,v2,c)

data = smooth3(Z,'gaussian',3);

data = permute(data,[2 1 3])./max(data(:));

% quarants = [0.05 0.1 0.2 0.4:0.1:0.7];
% quarants = [0.2];

%quarants = [0.2 0.4:0.1:0.7];

%quarants = [0.5:0.1:0.7];

%quarants = [0.4:0.1:0.7];

for quarant = 1:size(quarants,2)

%          patch(isocaps(data,quarants(1,quarant)),'FaceColor','interp','EdgeColor','none','FaceAlpha',(1/(size(quarants,2)^2))*quarant*quarant);
p1 = patch(isosurface(data,quarants(1,quarant)),'FaceColor',[1 0 0],'EdgeColor','none','FaceAlpha',(1/(size(quarants,2)^2))*quarant*quarant);

% isonormals(data,p1);
p1.FaceColor = c;
p1.EdgeColor = 'none';

end
view(v1,v2); 
axis vis3d;
% camlight light;
camlight headlight
colormap jet;
lighting gouraud;
box on;

set(gca,'XTickLabel',[])
set(gca,'XTick',[])
set(gca,'YTickLabel',[])
set(gca,'YTick',[])
set(gca,'ZTickLabel',[])
set(gca,'ZTick',[])
axis equal
set(gca,'color','none')


end

