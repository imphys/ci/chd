function [output_slice, output_total] = remap8Fold(superParticleSymm)

disp('REMAPPED WITH 8-FOLD SYMMETRY!')

for j = 1:length(superParticleSymm)
    par = superParticleSymm{j}; 

    %preRotate
    % angle = pi/8; 
    % par = par*[cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1]; 

    phi = atan2(par(:,2),par(:,1)); 
    phi_reduced = phi-mod(phi,pi/4);

    rotations = sort(unique(phi_reduced));
    for i = 1:length(rotations)
        angle = -rotations(i);
        idx = (phi_reduced==-angle);
        R = [cos(angle) sin(angle) 0; -sin(angle) cos(angle) 0; 0 0 1];
        par_remap(idx,:) = par(idx,:)*R; 
    end

    par_remap_total = [];
    for i = 1:length(rotations)
        angle = rotations(i); 
        R = [cos(angle) sin(angle) 0; -sin(angle) cos(angle) 0; 0 0 1];
        par_remap_total = [par_remap_total; par_remap*R]; 
    end

    output_slice{j} = par_remap; 
    output_total{j} = par_remap_total; 
end

end

