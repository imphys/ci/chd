%% Initialization
clear all 
close all
clc

path(pathdef)
addpath(genpath('build/mex/'))      %remove everything from path, except the correct mex files 

%initialize particles (S has two locs, M has 1)
S.points = [0,0,0;1,1,1];
S.sigma = [1 3;1 1];
M.points = [1 -1 1];
M.sigma = [10 1];

%test without rotation (RM=identity matrix) or a random rotation
RM = eye(3); 
% RM = rand(3);


%% Calculations
%this code assumes the new mex-files, where the indexing has been changed
%and the costfunction is normalized with respect to the sigmas. 
%If the old mex-files are used, the values will be different/wrong

%CPU
mex_expdist_cpu(S.points,M.points,S.sigma,M.sigma,RM)                 %should give 0.0742

%GPU
mex_expdist(S.points,M.points,reshape(S.sigma',2,2),M.sigma,RM)   %should give 0.0742
                %> note that we have to reshape sigmasA
                % on GitLab reshape(sigmasA',size(sigmasA,1),2) is replaces
                % by correct_uncer(sigmasA)

%correct manual normalized costFunction
cross_term = 0; 
for i = 1:size(S.points,1)
    for j = 1:size(M.points,1)
        r =(S.points(i,:)'-M.points(j,:)');
        Sa = diag([S.sigma(i,1) S.sigma(i,1) S.sigma(i,2)]);
        Sb = diag([M.sigma(j,1) M.sigma(j,1) M.sigma(j,2)]);
        Sb = RM*Sb*RM';    
        norm_factor = sqrt(det(Sa+Sb)); 
        exponent = r'*inv(Sa+Sb)*r;                 %r'*((Sa+Sb)\r) faster for big matrices
        cross_term = cross_term + exp(-exponent)/norm_factor;
    end
end
costValue = cross_term