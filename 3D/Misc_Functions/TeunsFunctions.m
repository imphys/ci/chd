clear all
% close all
clc
addpath(genpath('output'))

% outdir = '20200309_K_750_dataset_750_3D_NUP107_shift13_nph5000_dol50_3D_storm_ang30_r1';                   %1
% outdir = '20200309_K_750_dataset_750_shift13_nph5000_dol50_3D_storm_ang30_r1';
% outdir = '20200225_K_750_dataset_u2os_nup107_3D_r1';

outdir = 'output/20200225_K_750_dataset_u2os_nup107_3D_r1';

% outdir = 'output/20200302_K_500_dataset_500_3D_NUP107_shiftran_nph5000_dol50_3D_paint_ang30_r1';
% outdir = '20200225_K_200_dataset_100_shift13_100_shift0_nph5000_dol100_ang30_r1';
% outdir = 'oldCost/20200215_K_200_dataset_100_shift13_100_shift0_nph5000_dol100_ang30_r1';


K = str2double(extractBetween(outdir,'K_','_dataset')); 
load([outdir '/superParticleSymm.mat'])
load([outdir '/superParticle.mat'])
load([outdir '/subParticles'])
load([outdir '/Values'])  
load([outdir '/I_all2all'])  

% [superParticleSymm, superParticleSymmComplete] = remap8Fold(superParticleSymm); 

%% Visualize Particles
for i =1
        visualizeCloud3D(subParticles{i}.points,0.05, 1, num2str(i));
%     visualizeCloud3D(subParticles_slices{i}.points,0.05, 1, num2str(i));
%         visualizeCloud3DSurface_Teun(subParticles{i}.points,100);
%         par = subParticles{i}.points; 
%         figure()
%         scatter3(par(:,1),par(:,2),par(:,3),'.')
%         title(num2str(i))
%        axis square; axis equal
end

%% MakeMatrix 

[MatrixAfterAll2all, MatrixAfterAll2all_norm] = MakeMatrix3D(Values, I_all2all, subParticles);

mat = MatrixAfterAll2all_norm;
distanceMethod = 'complete';        %preferably complete!

numclust = 2; 
width =1.5;        %0.6 vs 1.5
threshold = 375; 

order = 1:K; 
names = MakeLeafNames(order,threshold); 
% names = MakeLeafNamesRadius(order,all_shifts);


clusters = makeTreeAndCluster(mat, distanceMethod, names, numclust,0);
clusters = clusters( cellfun(@(v) length(v),clusters) > 50); 
extracted = extractFromSuperParticle(subParticles, superParticle(end), num2cell(1:K));          %not sym is used!!  

%% Classification
%direct from Tree
superParticle_class_fromSP = extractFromSuperParticle(subParticles,superParticleSymm(end),clusters);   %use Symm
[eigenvectors,eigenvalues,x_mean,weights,Im] = eigenAnalysis3D(width, clusters, superParticle_class_fromSP );

KM = cluster(linkage(weights(:,1),'average'),'maxClust',2); 
clusters1{1} = sort(vertcat(clusters{KM==1}));              %clusters from tree, but merged in 2 
clusters1{2} = sort(vertcat(clusters{KM==2})); 
superParticle_class_fromSP1 = extractFromSuperParticle(subParticles,superParticleSymm,clusters1);   
[eigenvectors1,~,~,~,Im1] = eigenAnalysis3D(width, clusters1, superParticle_class_fromSP1);     %same as eigenvectors for numClust=2

%individual projection
[weights2,KM2,clusters2, ~] = projectOnEigenvectors3D(extracted,eigenvectors1,width);
superParticle_class_fromSP2 = extractFromSuperParticle(subParticles,superParticleSymm,clusters2);
[eigenvectors2,~,~,~,Im2] = eigenAnalysis3D(width, clusters2, superParticle_class_fromSP2);

%% many more...
%second round
[weights3,KM3,clusters3] = projectOnEigenvectors3D(extracted,eigenvectors2,width);
superParticle_class_fromSP3 = extractFromSuperParticle(subParticles,superParticleSymm,clusters3);
[eigenvectors3,~,~,~,Im3] = eigenAnalysis3D(width, clusters3, superParticle_class_fromSP3);

%third round...
[weights4,KM4,clusters4] = projectOnEigenvectors3D(extracted,eigenvectors3,width);
superParticle_class_fromSP4 = extractFromSuperParticle(subParticles,superParticleSymm,clusters4);
[eigenvectors4,~,~,~,Im4] = eigenAnalysis3D(width, clusters4, superParticle_class_fromSP4);

%fourth round...
[weights5,KM5,clusters5] = projectOnEigenvectors3D(extracted,eigenvectors4,width);
superParticle_class_fromSP5 = extractFromSuperParticle(subParticles,superParticleSymm,clusters5);
[eigenvectors5,~,~,~,Im5] = eigenAnalysis3D(width, clusters5, superParticle_class_fromSP5);

%fifth round...
[weights6,KM6,clusters6] = projectOnEigenvectors3D(extracted,eigenvectors5,width);
superParticle_class_fromSP6 = extractFromSuperParticle(subParticles,superParticleSymm,clusters6);
[eigenvectors6,~,~,~,Im6] = eigenAnalysis3D(width, clusters6, superParticle_class_fromSP6);

%sixth round...
[weights7,KM7,clusters7] = projectOnEigenvectors3D(extracted,eigenvectors6,width);
superParticle_class_fromSP7 = extractFromSuperParticle(subParticles,superParticleSymm,clusters7);
[eigenvectors7,~,~,~,Im7] = eigenAnalysis3D(width, clusters7, superParticle_class_fromSP7);

%% Performance
clc
for i = 1:2
    disp(['KM1: ' num2str(sum(vertcat(clusters{KM==i})<(threshold+1))) ' normal ' num2str(sum(vertcat(clusters{KM==i})>threshold)) ' mirror'] )
    disp(['KM2   : ' num2str(sum(find(KM2==i)<(threshold+1))) ' normal ' num2str(sum(find(KM2==i)>threshold)) ' mirror'] )
%     disp(['KM3      : ' num2str(sum(find(KM3==i)<(threshold+1))) ' normal ' num2str(sum(find(KM3==i)>threshold)) ' mirror'] )
%     disp(['KM4         : ' num2str(sum(find(KM4==i)<(threshold+1))) ' normal ' num2str(sum(find(KM4==i)>threshold)) ' mirror'] )
%     disp(['KM5            : ' num2str(sum(find(KM5==i)<(threshold+1))) ' normal ' num2str(sum(find(KM5==i)>threshold)) ' mirror'] )
%     disp(['KM6               : ' num2str(sum(find(KM6==i)<(threshold+1))) ' normal ' num2str(sum(find(KM6==i)>threshold)) ' mirror'] )
%     disp(['KM7                  : ' num2str(sum(find(KM7==i)<(threshold+1))) ' normal ' num2str(sum(find(KM7==i)>threshold)) ' mirror'] )
end

% wrong1 = sort( [ clusters1{1}((clusters1{1}>threshold));  clusters1{2}((clusters1{2}<(threshold+1))) ]);
% wrong2 = sort( [find(KM2((threshold+1):end)==2)+threshold; find(KM2(1:threshold)==1)]);
% wrong3 = sort( [find(KM3((threshold+1):end)==2)+threshold; find(KM3(1:threshold)==1)]);
% wrong4 = sort( [find(KM4((threshold+1):end)==2)+threshold; find(KM4(1:threshold)==1)]);
% wrong5 = sort( [find(KM5((threshold+1):end)==1)+threshold; find(KM5(1:threshold)==2)]);

% disp([mean(all_shifts(clusters1{1})) std(all_shifts(clusters1{1}))  mean(all_shifts(clusters1{2})) std(all_shifts(clusters1{2}))])
% disp([mean(all_shifts(clusters2{1})) std(all_shifts(clusters2{1}))  mean(all_shifts(clusters2{2})) std(all_shifts(clusters2{2}))])
% disp([mean(all_shifts(clusters3{1})) std(all_shifts(clusters3{1}))  mean(all_shifts(clusters3{2})) std(all_shifts(clusters3{2}))])
% disp([mean(all_shifts(clusters4{1})) std(all_shifts(clusters4{1}))  mean(all_shifts(clusters4{2})) std(all_shifts(clusters4{2}))])
% disp([mean(all_shifts(clusters5{1})) std(all_shifts(clusters5{1}))  mean(all_shifts(clusters5{2})) std(all_shifts(clusters5{2}))])
% disp([mean(all_shifts(clusters6{1})) std(all_shifts(clusters6{1}))  mean(all_shifts(clusters6{2})) std(all_shifts(clusters6{2}))])
% disp([mean(all_shifts(clusters7{1})) std(all_shifts(clusters7{1}))  mean(all_shifts(clusters7{2})) std(all_shifts(clusters7{2}))])

%% Plotting

%eigenvectors 
Z = (eigenvectors);   
eig1 = reshape(Z(:,1),100,100,100);
eig2 = reshape(Z(:,2),100,100,100); 

%z-slices
InteractiveMatrix(eig1)

%isosurfaces
InteractiveSurface(Z,'y',0.2);

%superParticles
% Z = makeImage3D(Particles_step1_sup,100,1); 
% Z = makeImage3D(initAlignedParticles_sup,100,1); 
Z = makeImage3D(superParticleSymm{end},50,1);
% Z = makeImage3D(subParticles{193}.points,100,1);
dipisosurface(smooth3(Z,'gaussian',5))

dipisosurface(Im1{2})
dipisosurface(eig1)


%merge isosurfaces 
close all
Z1 = makeImage3D(alignedClasses{2}.points,50,2);
Z2 = makeImage3D(alignedClasses{3}.points,50,2);
% Z1 = reshape(ImAll(:,18),100,100,100);
% Z1 = Im{6};
% Z2 = Im{1}; 
% Z1 = makeImage3D(MA.points,100,1); 
% Z2 = makeImage3D(MB.points,100,1); 
                % temp = zeros(1,35276); 
                % temp(1:round(end/2))=1; 
% Z1 = makeImage3D(superParticleSymmComplete{end}(logical(repmat(temp,1,8)),:),100,1); 
% Z2 = makeImage3D(superParticleSymmComplete{end}(logical(repmat(~temp,1,8)),:),100,1); 
% Z1 = makeImage3D(test{1}{1},100,1);
% Z2 = makeImage3D(test{2}{1},100,1);
[~,p1] =InteractiveSurface(Z1,'g',0.2);
[~,p2] =InteractiveSurface(Z2,'r',0.2);
fig = figure;
ax = axes; 
copyobj(p1,ax);
copyobj(p2,ax); 
ca=camlight('left');
set(ca,'style','infinite');
set(gca,'CameraViewAngleMode','manual')
axis equal



%dipImage
close all
dipisosurface(eig1)
dipisosurface(temp)
figure


visualizeCloud3D(superParticle_class_fromSP2{1}{end},0.05,1,'1');
visualizeCloud3D(superParticle_class_fromSP2{2}{end},0.05,1,'2');


% from clouds: 
visualizeCloud3DSurface_Teun(superParticleSymm{end}(17000:end,:),100)
visualizeSMLM3D(superParticle_class_fromSP2{1}{1},0.025,1); title 1
visualizeSMLM3D(superParticle_class_fromSP2{2}{1},0.05,1); title 2

Z = makeImage3D(superParticle{end},100,1);

InteractiveSurface(Z,'g')
dipisosurface(Z)


visualizeCloud3D_density(superParticle0{end},den0,'0')
visualizeCloud3D_density(superParticle13{end},den13,'13')

%difference in density 
figure()
plot(superParticleSymm{end}(:,3),density_superP_Symm,'.')
grid 
grid minor

%% Shift histograms

temp = clusters; 

figure
histogram(all_shifts(temp{1}),linspace(min(all_shifts),max(all_shifts),7))
hold on
histogram(all_shifts(temp{2}),linspace(min(all_shifts),max(all_shifts),7))
xlabel('radius (nm)')
ylabel('count')
title('Radius distribution per class (clusters1)')

%theoretical limit
med = median(all_shifts);
disp([mean(all_shifts(all_shifts<med)) std(all_shifts(all_shifts<med)) mean(all_shifts(all_shifts>med)) std(all_shifts(all_shifts>med))])

%% MDS 

%% test MDS on old datasets
[MatrixAfterAll2all, MatrixAfterAll2all_norm] = MakeMatrix3D(Values, I_all2all, subParticles);
for i = 1:K
%     subParticles_extr{i}.points = extracted{i}{1};
%     subParticles_extr{i}.sigma = subParticles{i}.sigma; 
    subParticles_extr{i}.points = NUP107Particles{i}.points; 
    subParticles_extr{i}.sigma = NUP107Particles{i}.sigma;
end
[MatrixAfterAll2all, MatrixAfterAll2all_norm] = MakeMatrixAfterBootstrap3D(subParticles_extr); 


% D = MatrixAfterAll2All+MatrixAfterAll2All';
D = MatrixAfterAll2all_norm+MatrixAfterAll2all_norm';
D = max(D(:))-D; 
D = D - diag(diag(D)); 
% mds = cmdscale(D);        %mdscale
mds = mdscale(D,30,'Criterion','metricstress');

[c] = kmeans(mds(:,1:end),2,'replicates',1000);
% c = cluster(linkage(mds(:,1:3),'complete'),'maxClust',2); 
% 
% lab = zeros(1,K);
% lab(1:threshold) = 1; 

figure(); scatter3(mds(:,1),mds(:,2),mds(:,3),'o','filled')
figure(); scatter3(mds(:,1),mds(:,2),mds(:,3),[],c,'o','filled')

clear clusters
for i = 1:max(c)
clusters{i} = find(c==i);
end


super1 = [];
super2 = [];
for i = clusters{1}'
    super1 = [super1; subParticles_extr{i}.points];
end
for i = clusters{2}'
    super2 = [super2; subParticles_extr{i}.points];
end

den1 = visualizeCloud3D(super1,0.05, 1, '1');
den2= visualizeCloud3D(super2,0.05, 1, '2');



