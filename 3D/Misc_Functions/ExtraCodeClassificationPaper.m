clc

% par = [];
% sigmas = [];
% for i = 1:300
%     par = [par;NUP107Particles{i}.points ];
%     sigmas = [sigmas; NUP107Particles{i}.sigma];
% end
% 

% den1 = visualizeCloud3D(par,0.05, 1, 'all');

%% [1] Make Directory
USE_GPU_GAUSSTRANSFORM = 1;
USE_GPU_EXPDIST = 1;

% dataset = 'NUP96_STORM_315x_Jonas.mat';
% K = 315; 
% dataset = 'NUP107_STORM_300x_Jonas.mat';
% K = 300; 
% dataset = 'NUP107_PAINT_306x_Ralf.mat';
% K = 306; 
dataset = 'tetradouble.mat';
K=218;
repeat = 1; 
load(dataset)
outdir = ['output/' datestr(now,'yyyymmdd_') 'K_' num2str(K) '_dataset_' extractBefore(dataset,".") '_r' num2str(repeat)];

if ~exist(outdir,'dir')
    mkdir(outdir);
else
    disp('ERROR: outdir exists')
end  

%% [2] All2all

% subParticles = particles; 
scale = 0.2; 

save([outdir '/subParticles'],'subParticles')
save([outdir '/scale'],'scale')
save([outdir '/outdir'],'outdir')

%SCALE!
[RR_all2all, I_all2all, Values] = all2all3D(subParticles, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST, outdir,scale);

%% [3] Classification MDS
[MatrixAfterAll2all, MatrixAfterAll2all_norm] = MakeMatrix3D(Values, I_all2all, subParticles);

D = MatrixAfterAll2all_norm+MatrixAfterAll2all_norm';
D = max(D(:))-D; 
D = D - diag(diag(D)); 
mds = mdscale(D,30,'Criterion','metricstress');

% [c] = kmeans(mds(:,1:end),8,'replicates',1000);         %NUMBER OF CLUSTERS

% figure(); scatter3(mds(:,1),mds(:,2),mds(:,3),'o','filled')
figure(); scatter3(mds(:,1),mds(:,2),mds(:,3),[],c,'o','filled')

% clear clusters
% for i = 1:max(c)
%     clusters{i} = find(c==i);
% end

% save([outdir '/results8/clusters'],'clusters')          %NUMBER OF CLUSTERS

%% Per class reconstruction
% 
% for c = 1:length(clusters)
% 
%     %only take clusters connections
%     Nc = length(clusters{c});
%     connected = zeros(1,size(I_all2all,2));
%     for i = 1:size(I_all2all,2)
%         if length(intersect(clusters{c},I_all2all(:,i)))==2
%             connected(i)=1; 
%         end
%     end
%     indices_connected = find(connected); 
%     
%     RR_all2all_clusters = RR_all2all(:,:,indices_connected); 
%     I_all2all_clusters = I_all2all(:,indices_connected); 
%     
%     %correct values in I_all2all
%     for i = 1:numel(I_all2all_clusters)
%         I_all2all_clusters(i) = find(clusters{c} == I_all2all_clusters(i));
%     end
%     
%     
%     % 2-1 Lie-algebraic averaging
%     maxIters = 50; 
%     disp('1st Lie-algebraic averaging started!')
%     [Mest,~,scores1] = MeanSE3Graph(RR_all2all_clusters, I_all2all_clusters,maxIters);
%     % 2-2 registration consistency check
%     [RR_outRem, I_outRem] = consistencyCheck(Mest, RR_all2all_clusters, I_all2all_clusters, Nc);
%     % 2-3 second Lie-algebraic averaging
%     disp('2nd Lie-algebraic averaging started!');
%     [M_new,~,scores2] = MeanSE3Graph(RR_outRem,I_outRem,maxIters);
% 
%     % 2-4 make the first data-driven template
% %     [Particles_step1, Particles_step1_sup] = makeTemplate(Mest, subParticles(clusters{c}));                   %after Lie1
%     [initAlignedParticles{c}, initAlignedParticles_sup{c}] = makeTemplate(M_new, subParticles(clusters{c}));    %after Lie2
%     
%     
%     %Bootstrapping
%     USE_SYMMETRY = 0;   % flag for imposing symmetry prio knowledge
%     M1 = [];            % not implemented
%     iter = 5;           % number of iterations
%     [super{c}, ~] = one2all3D(initAlignedParticles{c}, iter, scale, M1, outdir, initAlignedParticles_sup{c}, USE_SYMMETRY, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST);
% end
% 
% save([outdir '/results8/initAlignedParticles'],'initAlignedParticles')
% save([outdir '/results8/initAlignedParticles_sup'],'initAlignedParticles_sup')
% save([outdir '/results8/super'],'super')
% 
% % for i = 1%:length(clusters)
% %     density_superP = visualizeCloud3D(super{i}{end},0.1, 1,['class ' num2str(i) ' (' num2str(length(clusters{i})) ' particles)']);
% %     axis equal
% % end
% 
% % figure()
% % scatter3(super{1}{end}(:,1),super{1}{end}(:,2),super{1}{end}(:,3),0.1,'.')
% 
% 
% %% [5] Compare with Eigen
% for i = 1:length(super)
%     superParticle_reconPerClass{i}{1} = alignedClasses{i}.points;
% end
% [eigenvectors,eigenvalues,x_mean,weights,Im] = eigenAnalysis3D(width, clusters, superParticle_reconPerClass);
% 
% Z = (eigenvectors); 
% 
% L = 25; 
% Z = makeImage3D(super{6}{end},L,1.5);
% eig1 = reshape(Z,L,L,L);
% % InteractiveMatrix(eig1)
% InteractiveSurface(eig1,'r',0.02)
% 
% %% [6] align classes (2)
% 
% i=1; j=2; 
% [param, val] = pairFitting3D(subParticles_clustered_sub{1,i}.points, subParticles_clustered_sub{1,j}.points, ...
%                                subParticles_clustered_sub{1,i}.sigma, subParticles_clustered_sub{1,j}.sigma, scale, 1, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST,4);
% 
%           alignedClasses{2}.points = subParticles_clustered{2}.points;                  
% alignedClasses{1}.points = rotate_by_pifourth3d(transform_by_rigid3d(subParticles_clustered{i}.points, param));
% 
% %% [6] align classes (3 or more)
% for i = 1:length(clusters)
%     members = clusters{i}';
%     sigmas = [];
%     for m = members
%         sigmas = [sigmas; subParticles{m}.sigma];
%     end
%     
%     subParticles_clustered{i}.points = super{i}{end}; 
%     subParticles_clustered{i}.sigma = sigmas; 
%     
%      idx = randi(size(subParticles_clustered{i}.points,1),500,1);       %only used for all2all
%      subParticles_clustered_sub{i}.points = subParticles_clustered{i}.points(idx,:);
%      subParticles_clustered_sub{i}.sigma = subParticles_clustered{i}.sigma(idx,:);
% end
% 
% 
% %reconstruct (/align)
% scale = 0.1;        %0.2 for tetrahedron, 0.1 for NUP
% [RR_align, I_align, ~] = all2all3D(subParticles_clustered_sub, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST, 0,scale);   %outdir 0 to not save outputs
% 
% maxIters = 50; 
% [Mest_align,~,~] = MeanSE3Graph(RR_align, I_align,maxIters);
% [RR_outRem_align, I_outRem_align] = consistencyCheck(Mest_align, RR_align, I_align, length(subParticles_clustered));
% [M_new_align,~,~] = MeanSE3Graph(RR_outRem_align,I_outRem_align,maxIters);
% [alignedClasses, alignedClasses_sup] = makeTemplate(M_new_align, subParticles_clustered);   
%         % I do all2all on subsampled, and then LIe on full particles 
% 
% 
% %% [7] plotting together (2x)
% close all
% %Tetrahedron: L=25, alpha=0.03
% L = 25; 
% % Z1 = makeImage3D(super{1}{end},L,1.5);
% % Z2 = makeImage3D(super{2}{end},L,1.5);
% % Z1 = makeImage3D(M_trans,L,1.5);
% % Z2 = makeImage3D(S.points,L,1.5);
% 
% Z1 = makeImage3D(alignedClasses{1}.points,L,2);
% Z2 = makeImage3D(alignedClasses{2}.points,L,2);
% 
% [~,p1] =InteractiveSurface(Z1,'r',0.06);
% [~,p2] =InteractiveSurface(Z2,'y',0.06);
% fig = figure;
% ax = axes; 
% copyobj(p1,ax);
% copyobj(p2,ax); 
% ca=camlight('left');
% set(ca,'style','infinite');
% set(gca,'CameraViewAngleMode','manual')
% axis equal
% 
% %% plotting together (3x)
% close all
% 
% L = 25; 
% Z1 = makeImage3D(alignedClasses{8}.points,L,1.5);
% Z2 = makeImage3D(alignedClasses{7}.points,L,1.5);
% Z3 = makeImage3D(alignedClasses{6}.points,L,1.5);
% % Z4 = makeImage3D(alignedClasses{4}.points,L,1.5);
% % Z5 = makeImage3D(alignedClasses{5}.points,L,1.5);
% 
% 
% [~,p1] =InteractiveSurface(Z1,'g',0.06);
% [~,p2] =InteractiveSurface(Z2,'r',0.06);
% [~,p3] =InteractiveSurface(Z3,'y',0.06);
% % [~,p4] =InteractiveSurface(Z4,'c',0.06);
% % [~,p5] =InteractiveSurface(Z5,'k',0.03);
% 
% 
% fig = figure;
% ax = axes; 
% copyobj(p1,ax);
% copyobj(p2,ax); 
% copyobj(p3,ax); 
% % copyobj(p4,ax); 
% % copyobj(p5,ax); 
% ca=camlight('left');
% set(ca,'style','infinite');
% set(gca,'CameraViewAngleMode','manual')
% axis equal
% close(figure(1))
% close(figure(2))
% close(figure(3))
% 
% %% [8] align 2 (not used)
% 
% M = subParticles_clustered{2}; 
% S = subParticles_clustered{3}; 
% 
% N = 200; 
% idxM = randi(size(M.points,1),N,1); 
% idxS = randi(size(S.points,1),N,1); 
% M_sub.points = M.points(idxM,:);
% M_sub.sigma = M.sigma(idxM,:); 
% S_sub.points = S.points(idxS,:); 
% S_sub.sigma = S.sigma(idxS,:);
% 
% 
% % M = subParticles{1};
% % S = subParticles{2}; 
% 
% [param, val] = pairFitting3D(M_sub.points, S_sub.points, M_sub.sigma, S_sub.sigma, scale, 1, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST,4);
% % [param, val] = pairFitting3D(M.points, S.points, M.sigma, S.sigma, scale, 1, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST,4);
% M_trans = transform_by_rigid3d(M.points,param); 
% 
% figure()
% scatter3(M_trans(:,1),M_trans(:,2),M_trans(:,3),'.')
% % scatter3(M.points(:,1),M.points(:,2),M.points(:,3))
% hold on
% scatter3(S.points(:,1),S.points(:,2),S.points(:,3),'.')
% 
% %% Align all to 1
% 
% A = subParticles_clustered{2}; 
% B = subParticles_clustered{3}; 
% C = subParticles_clustered{4}; 
% D = subParticles_clustered{5};
% E = subParticles_clustered{1}; 
% 
% N = 500; 
% idxA= randi(size(A.points,1),N,1); 
% A_sub.points = A.points(idxA,:);
% A_sub.sigma = A.sigma(idxA,:); 
% 
% idxB= randi(size(B.points,1),N,1); 
% B_sub.points = B.points(idxB,:);
% B_sub.sigma = B.sigma(idxB,:); 
% 
% idxC= randi(size(C.points,1),N,1); 
% C_sub.points = C.points(idxC,:);
% C_sub.sigma = C.sigma(idxC,:); 
% 
% idxD= randi(size(D.points,1),N,1); 
% D_sub.points = D.points(idxD,:);
% D_sub.sigma = D.sigma(idxD,:); 
% 
% idxE= randi(size(E.points,1),N,1); 
% E_sub.points = E.points(idxE,:);
% E_sub.sigma = E.sigma(idxE,:); 
% 
% [paramBA, val] = pairFitting3D(B_sub.points, A_sub.points, B_sub.sigma, A_sub.sigma, scale, 1, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST,4);
% [paramCA, val] = pairFitting3D(B_sub.points, A_sub.points, C_sub.sigma, A_sub.sigma, scale, 1, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST,4);
% [paramDA, val] = pairFitting3D(B_sub.points, A_sub.points, D_sub.sigma, A_sub.sigma, scale, 1, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST,4);
% [paramEA, val] = pairFitting3D(B_sub.points, A_sub.points, E_sub.sigma, A_sub.sigma, scale, 1, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST,4);
% 
% B_trans = transform_by_rigid3d(B.points,paramBA); 
% C_trans = transform_by_rigid3d(C.points,paramCA); 
% D_trans = transform_by_rigid3d(D.points,paramDA); 
% E_trans = transform_by_rigid3d(E.points,paramEA); 
% 
% 
% close all
% L = 25; 
% Z1 = makeImage3D(A.points,L,2);
% Z2 = makeImage3D(B_trans,L,2);
% Z3 = makeImage3D(C_trans,L,2);
% Z4 = makeImage3D(D_trans,L,2);
% % Z5 = makeImage3D(E_trans,L,2);
% 
% [~,p1] =InteractiveSurface(Z1,'g',0.03);
% [~,p2] =InteractiveSurface(Z2,'r',0.03);
% [~,p3] =InteractiveSurface(Z3,'y',0.03);
% [~,p4] =InteractiveSurface(Z4,'c',0.03);
% % [~,p5] =InteractiveSurface(Z5,'k',0.03);
% 
% 
% fig = figure;
% ax = axes; 
% copyobj(p1,ax);
% copyobj(p2,ax); 
% copyobj(p3,ax); 
% copyobj(p4,ax); 
% % copyobj(p5,ax); 
% ca=camlight('left');
% set(ca,'style','infinite');
% set(gca,'CameraViewAngleMode','manual')
% axis equal
% 
% %% superPar to particle
% clear A_sub B_sub 
% clc
% A = superParticle{end};
% B = alignedClasses{1};
% 
% sigmas = [];
% for i = 1:length(subParticles)
%     sigmas = [sigmas; subParticles{i}.sigma];
% end
% 
% N = 500; 
% idxA = randi(size(A,1),N,1); 
% A_sub = A(idxA,:);
% sigmas = sigmas(idxA,:);
% idxB = randi(size(B.points,1),N,1); 
% B_sub.points = B.points(idxB,:);
% B_sub.sigma = B.sigma(idxB,:); 
% 
% [paramAB, val] = pairFitting3D(A_sub, B_sub.points, sigmas, B_sub.sigma, scale, 1, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST,4);
% A_trans = transform_by_rigid3d(A,paramAB); 
% 
% scatter3(A_trans(:,1),A_trans(:,2),A_trans(:,3),'.')
% 
% %% z-distribution
% close all
% figure()
% bins = linspace(-100,100,100); 
% for i = 1:3
% z = alignedClasses{i}.points(:,3);
% % z = super{i}{end}(:,3);
% z = z*130; 
% h=histogram(z,bins);
% h.EdgeAlpha= 0; 
% hold on
% end
% grid; grid minor
% xlabel 'z-position (nm)'
% ylabel 'count'
% 
% 
% % figure()
% % z = A_trans(:,3);
% % z = z*130; 
% % h=histogram(z,bins);
% % h.EdgeAlpha= 0; 
% % grid; grid minor
% % xlabel 'z-position (nm)'
% % ylabel 'count'
% 
% %% check individual particles for 3-dots
% close all
% figure()
% teller = 1; 
% for i = 4
% %     subplot(3,3,teller)
%     teller = teller+1; 
%     par = subParticles{i}.points; 
%     scatter3(par(:,1),par(:,2),par(:,3),'.')
%     xlabel x; ylabel y; zlabel z; title(num2str(i));
%     zlim([-1 1])
% end
% 
% %% show surf of superParticle
% close all
% L = 50; 
% % Zsuper= makeImage3D(superParticle{end},L,1.5);
% Zsuper= makeImage3D(super{6}{end},L,1.5);   
% InteractiveSurface(Zsuper,'g',0.06);
% 
% 
% 
% 
