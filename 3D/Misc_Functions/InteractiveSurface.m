function [f,p1]= InteractiveSurface(Z,c,startIso)

f = figure; 
ax = axes('Parent',f,'position',[0.13 0.25  0.7 0.70]);

% ax = axes('Parent',f,'position',[0.13 0.39  0.77 0.54]);
% i = floor(size(Z,1)/2); 
[v1,v2] = view(3); 
p1 = visualizeMatrix3DSurface(Z,startIso,v1,v2,c); 
axis off
colormap hot

b = uicontrol('Parent',f,'Style','slider','Position',[81,54,419,23],...
              'value',startIso, 'min',0, 'max',0.2);
bgcolor = f.Color;
bl1 = uicontrol('Parent',f,'Style','text','Position',[50,54,23,23],...
                'String','0','BackgroundColor',bgcolor);
bl2 = uicontrol('Parent',f,'Style','text','Position',[500,54,23,23],...
                'String','1','BackgroundColor',bgcolor);
bl3 = uicontrol('Parent',f,'Style','text','Position',[190,25,200,23],...
                'String',['IsoValue = ' num2str(startIso)],'BackgroundColor',bgcolor);
            
b.Callback = {@callback,Z,bl3};
% addlistener(b,'Value',@callback)

function callback(h,event,Z,bl3) 
        delete(findobj('type','Light'));
        delete(findobj('type','patch'));
        [v1,v2] = view; 
        visualizeMatrix3DSurface(Z,h.Value,v1,v2,c); 
        bl3.String = ['IsoValue = ' num2str(h.Value)];
end

end



            

