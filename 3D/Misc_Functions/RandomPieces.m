%% Rotating View

% set(gca,'XColor','none')
% set(gca,'YColor','none')
% set(gca,'ZColor','none')

set(gca,'CameraViewAngleMode','manual')
for i = linspace(0,360,100)
    view(i,i/4)
    pause(0.03)
end

%% Concatenate all subParticles

conCatSubPar = [];
for i = 1:K
       conCatSubPar = [conCatSubPar; subParticles{i}.points]; 
end

%% Make Subset

load('750_3D_NUP107_shift13_nph5000_dol50_3D_storm_ang30.mat')
particles1 = particles; 
clear particles 

% load('500_3D_NUP107_shift0_nph5000_dol25_3D_paint_ang30.mat')
% particles2 = particles; 
% clear particles
% 
% load('20_FTU.mat')
% particles3 = particles; 
% clear particles

particles = {particles1{randperm(750,250)}};% particles2{randperm(500,250)}};
% particles = {particles1{:} particles2{:}};
clear particles1 particles2 

save('data/250_shift13_nph5000_dol50_3D_storm_ang30','particles')

%% Check flipping
% close all

%initial orientation of subParticles
for i = 1:length(particles)
    param = particles{i}.param; 
    q = [param(4) param(1) param(2) param(3)];
    q_initial{i} = q2R(q); 
%     init_orientation(i) = q(3,3);  
    vec_initial(:,i) = q_initial{i}*[0;0;1];

    r = norm(vec_initial(:,i));
    phi_initial(i) = acos(vec_initial(3,i)/r); 
end
% 
% figure
% plot(phi_initial,'.')
% grid
% grid minor
% title initial
% 
% figure()
% scatter3(vec_initial(1,:),vec_initial(2,:),vec_initial(3,:),'.')
% title initial
% axis equal


%orientation in superParticle
% [~,MT] = get_final_transform_params(superParticleSymm{6},subParticles);
% [~,MT] = get_final_transform_params(superParticleUpperFlipped_sup,subParticles);
[~,MT] = get_final_transform_params(Flipped_sup_reboot{2}, extractedBoth2);
for i = 1:length(subParticles)
    q_final{i} = MT{i}.T; 
    vec_final(:,i) = q_final{i}*[1;1;1];
    r = norm(vec_final(:,i));
    phi_final(i) = acos(vec_final(3,i)/r); 
end

figure()
scatter3(vec_final(1,:),vec_final(2,:),vec_final(3,:),'.')
title final
axis equal
% 
% 
% % final_z_orientation = cellfun(@(v) v.T(3,3),MT);
% figure()
% plot(phi_final,'.')
% grid 
% grid minor
% title final


%together
% figure
% scatter3(vec_initial(1,:),vec_initial(2,:),vec_initial(3,:),'r.')
% hold on 
% scatter3(vec_final(1,:),vec_final(2,:),vec_final(3,:),'b.')
% title final
% legend('initial','fina')
% axis equal


%total
for i = 1:length(particles)
%     vec_total(:,i) = (q_initial{i}*q_final{i})*[0;0;1];
    vec_total(:,i) = q_final{i}*(q_initial{i}*[0;0;1]);
    r = norm(vec_total(:,i));
    phi_total(i) = acos(vec_total(3,i)/r); 
end

figure()
scatter3(vec_total(1,:),vec_total(2,:),vec_total(3,:),'.')
view(3)
title total
axis equal

%% Check if lower ring is denser
clc

% in subParticles
locs = [];
for i = 1:length(subParticles)
    locs = [locs; subParticles{i}.points];
end

figure()
hist(locs_superp_symm(:,3),100)

perc_lower = sum(locs(:,3)<0)/size(locs,1);            % 51.5%
perc_upper = sum(locs(:,3)>=0)/size(locs,1);        % 48.5

%in superParticle
locs_superp_symm = superParticleSymm{6}; 
perc_lower_super = sum(locs_superp_symm(:,3)<0)/size(locs_superp_symm,1);       %  51.4% 
perc_upper_super = sum(locs_superp_symm(:,3)>=0)/size(locs_superp_symm,1);   %  48.6%

%see difference between classes
locs = [];
for i = clusters2{1}'
    locs = [locs; subParticles{i}.points];
end
perc_lower = sum(locs(:,3)<0)/size(locs,1)      
perc_upper = sum(locs(:,3)>=0)/size(locs,1)      

%% Model to subPar, then to superPar
%> to see how model is oriented in superParticle 
close all

[~,MT] = get_final_transform_params(superParticle{6},subParticles);
figure()

sr = [];
for i=1:100%:numel(particles)

        % fetch the transformation parameters
        tmpParam = particles{1,i}.param;
        if i<101
            load('Model_NUP107_shift13.mat');
        else
            load('Model_NUP107_shift0.mat');
        end
        par = Model; 
        
        % second rotate
        gtParam2 = [tmpParam(1),tmpParam(2),tmpParam(3),tmpParam(4), 0, 0, 0];      %removed the minus sign? 
        tmpParticle = transform_by_rigid3d(par, gtParam2);
        
        % first translate
        gtParam1 = [0, 0, 0, 1, ...
                   tmpParam(5),tmpParam(6),tmpParam(7)];           
        tmpParticle = transform_by_rigid3d(tmpParticle, gtParam1);
        
        tmpParticle = tmpParticle * (MT{i}.T); 
        
%         sr = [sr;tmpParticle];
        if i<101
                    scatter3(tmpParticle(:,1),tmpParticle(:,2),tmpParticle(:,3),'b.')
        else
                    scatter3(tmpParticle(:,1),tmpParticle(:,2),tmpParticle(:,3),'r.')

        end
        
        hold on
end
%     figure()
%     scatter3(sr(:,1),sr(:,2),sr(:,3),'.')
%     title ModelForward
% %     view(0,0)
    
    
% figure()
% par = subParticles{i}.points; 
% scatter3(par(:,1),par(:,2),par(:,3))
% view(0,0)
    
%% TEST procustes
close all

figure()
par1 = subParticles{2}.points;
scatter3(par1(:,1),par1(:,2),par1(:,3))
title par1

figure()
par1 = superParticleWithoutPK{6}(714:1458,:);
scatter3(par1(:,1),par1(:,2),par1(:,3))
title parFromSup

figure()
par1 = subParticles{2}.points * MT{2}.T; 
scatter3(par1(:,1),par1(:,2),par1(:,3))
title rotatedSubPar

figure()
par1 = superParticleWithoutPK{6}(714:1458,:)* inv(MT{2}.T);
scatter3(par1(:,1),par1(:,2),par1(:,3))
title rotatedsuperP

%% Plot Model
close all

load('Model_NUP107_shift13.mat')

figure()
draw = 1:16; 

scatter3(Model(draw,1),Model(draw,2),Model(draw,3),'filled')
hold on
draw = 17:32;
scatter3(Model(draw,1),Model(draw,2),Model(draw,3),'r','filled')

% axis equal
% view(90,90)
% xlim([-0.5 0.5])
% ylim([-0.5 0.5]) 
% zlim([-0.3 0.3])
% yticks([-0.5:0.5:0.5])
% xticks([-0.5:0.5:0.5])
% zticks([-0.3:0.15:0.3])
% grid off
% set(gca,'Color','None')

Model2 = Model*[1 0 0; 0 -1 0; 0 0 -1]; 
figure()
draw = 1:16; 
scatter3(Model2(draw,1),Model2(draw,2),Model2(draw,3),'filled')
hold on
draw = 17:32;
scatter3(Model2(draw,1),Model2(draw,2),Model2(draw,3),'r','filled')

%% See which particles are 'bad'

figure
numLocs = cellfun(@(v) size(v.points,1), subParticles);
plot(numLocs,'.')
grid 
grid minor

figure
sitesLabeled = cellfun(@(v) size(v.binding_sites,1), particles);
plot(sitesLabeled,'.')
grid
grid minor

%% Manual costFunction calculation 
clear output output2 dist dist2 val testDistA testDistB
clc

a=1; 
b=2; 
scale = 0.1; 
[par, val] = pairFitting3D(subParticles{a}.points, subParticles{b}.points, ...
                               subParticles{a}.sigma, subParticles{b}.sigma, scale, 1, 1, 1,4);

M = subParticles{a};
S = subParticles{b};

M_points_transformed = transform_pointset(M.points, 'rigid3d', par);
RM = quaternion2rotation(par(1:4));     %M>S

tic
cost = mex_expdist(S.points, M_points_transformed, reshape(S.sigma',size(S.sigma,1),2), M.sigma, RM)
toc
cost = mex_expdist_cpu(S.points, M_points_transformed, S.sigma, M.sigma, RM)
toc
                           



cross_term = 0; 
for i = 1:size(S.points,1)
    for j = 1:size(M.points,1)
        r =(S.points(i,:)'-M_points_transformed(j,:)');
        Sa = diag([S.sigma(i,1) S.sigma(i,1) S.sigma(i,2)]);
        Sb = diag([M.sigma(j,1) M.sigma(j,1) M.sigma(j,2)]);
        Sb = RM*Sb*RM';     %???
        nor = sqrt(det(Sa+Sb)); 
        exponent = r'*inv(Sa+Sb)*r;
        cross_term = cross_term + exp(-exponent)/nor; 
    end
end
cross_term

%% TestMex
S = subParticles{1}; 
M = subParticles{2}; 

RM = eye(3); 
RM = [0 0 1;1 0 0; 0 1 0]; 
RM = rand(3); 

cross_term = 0; 
for i = 1:size(S.points,1)
    for j = 1:size(M.points,1)
        r =(S.points(i,:)'-M.points(j,:)');
        Sa = diag([S.sigma(i,1) S.sigma(i,1) S.sigma(i,2)]);
        Sb = diag([M.sigma(j,1) M.sigma(j,1) M.sigma(j,2)]);
        Sb = RM*Sb*RM';     %???
        nor = sqrt(det(Sa+Sb)); 
        exponent = r'*inv(Sa+Sb)*r;     %r'*((Sa+Sb)\r) faster 
        cross_term = cross_term + exp(-exponent)/nor;
    end
end
cross_term

tot = 10; 
mex_expdist_cpu(S.points(1:tot,:),M.points(1:tot,:),S.sigma(1:tot,:), M.sigma(1:tot,:), RM)
mex_expdist(S.points(1:tot,:),M.points(1:tot,:),reshape(S.sigma(1:tot,:)',tot,2), M.sigma(1:tot,:), RM)


%full
mex_expdist_cpu(S.points,M.points,S.sigma, M.sigma, RM)
mex_expdist(S.points,M.points,reshape(S.sigma',size(S.sigma,1),2), M.sigma, RM)

%% Testing if normalization works in MexFiles 

path(pathdef)
addpath(genpath('build/mex/'))
% addpath(genpath('buildOld/mex/'))

clc
S.points = [0,0,0;1,1,1];
S.sigma = [1 3;1 1];
M_points_transformed = [1 -1 1];
M.sigma = [10 1];
RM = eye(3); 
% RM = rand(3);



one = mex_expdist_cpu(S.points(1,:),M_points_transformed,S.sigma(1,:),M.sigma,RM)
two = mex_expdist_cpu(S.points(2,:),M_points_transformed,S.sigma(2,:),M.sigma,RM)
both = mex_expdist_cpu(S.points(1:2,:),M_points_transformed,S.sigma(1:2,:),M.sigma,RM)

one = mex_expdist(S.points(1,:),M_points_transformed,S.sigma(1,:),M.sigma,RM)
two = mex_expdist(S.points(2,:),M_points_transformed,S.sigma(2,:),M.sigma,RM)
both = mex_expdist(S.points(1:2,:),M_points_transformed,correct_uncer(S.sigma(1:2,:)),M.sigma,RM)


%to make old work 
Sa = reshape(S.sigma(1:2,:)',2,2); 
mex_expdist_cpu(S.points(1:2,:),M_points_transformed,Sa,M.sigma,RM)



cross_term = 0; 
for i = 1:size(S.points,1)
    for j = 1:size(M_points_transformed,1)
        r =(S.points(i,:)'-M_points_transformed(j,:)');
        Sa = diag([S.sigma(i,1) S.sigma(i,1) S.sigma(i,2)]);
        Sb = diag([M.sigma(j,1) M.sigma(j,1) M.sigma(j,2)]);
        Sb = RM*Sb*RM';     %???
        nor = sqrt(det(Sa+Sb)); 
        exponent = r'*inv(Sa+Sb)*r;     %r'*((Sa+Sb)\r) faster 
        cross_term = cross_term + exp(-exponent)/nor;
    end
end
cross_term

%% testin Subtomogram averaging
close all

[par_remap par_remap_total] = remap8Fold(superParticleSymm);

% figure
% scatter3(par_remap(:,1),par_remap(:,2),par_remap(:,3),'.')
% view(0,90)
% 
% figure
% scatter3(par(:,1),par(:,2),par(:,3),'.')
% view(0,90)

temp = makeImage3D(superParticleSymm{end},100,1);
InteractiveSurface(temp,'r');
title original

temp = makeImage3D(par_remap_total{end},100,1);
InteractiveSurface(temp,'r');
title remapTotal

%% use remap8fold to recalculate Matrix
len = cellfun(@(v) size(v.points,1), subParticles); 
len_all = sum(len); 
clear subParticles_slices

%subParticles_slices
for i = 1:K
    subParticles_slices{i}.points = [];
    for j = 1:8
        x= {superParticleSymmComplete{end}((1:len_all)+(j-1)*len_all,:)};
        temp = extractFromSuperParticle(subParticles,x , {i}); 
        subParticles_slices{i}.points = [subParticles_slices{i}.points; temp{1}{1}];
    end
    subParticles_slices{i}.sigma = repmat(subParticles{i}.sigma,8,1); 
end


[MatrixAfterBootstrap_remap,MatrixAfterBootstrap_remap_norm] = MakeMatrixAfterBootstrap3D(subParticles_slices);
figure()
imagesc(MatrixAfterBootstrap_remap_norm)

%% remap8fold: clustering
mat = MatrixAfterBootstrap_remap_norm;
distanceMethod = 'complete';

numclust = 10; 
threshold = 375; 
order = 1:K; 
names = MakeLeafNames(order,threshold); 
% names = MakeLeafNamesRadius(order,all_shifts);

clusters = makeTreeAndCluster(mat, distanceMethod, names, numclust,0);
clusters = clusters( cellfun(@(v) length(v),clusters) >5); 

%% remap8fold: convert to superParticle_slices

superParticleSymm_slices = [];
for i = 1:K
    superParticleSymm_slices = [superParticleSymm_slices; subParticles_slices{i}.points]; 
end
extracted = extractFromSuperParticle(subParticles_slices, {superParticleSymm_slices}, num2cell(1:K));          %not sym is used!!  

%% remap8fold: projections 
superParticle_class_fromSP = extractFromSuperParticle(subParticles_slices,{superParticleSymm_slices},clusters);   

[eigenvectors,eigenvalues,x_mean,weights,Im] = eigenAnalysis3D(width, clusters, superParticle_class_fromSP );

KM = cluster(linkage(weights(:,1),'average'),'maxClust',2); 
clusters1{1} = sort(vertcat(clusters{KM==1}));              %clusters from tree, but merged in 2 
clusters1{2} = sort(vertcat(clusters{KM==2})); 
superParticle_class_fromSP1 = extractFromSuperParticle(subParticles_slices,{superParticleSymm_slices},clusters1);   
[eigenvectors1,~,~,~,Im1] = eigenAnalysis3D(width, clusters1, superParticle_class_fromSP1);     %same as eigenvectors for numClust=2

[weights2,KM2,clusters2, ~] = projectOnEigenvectors3D(extracted,eigenvectors1,width);
superParticle_class_fromSP2 = extractFromSuperParticle(subParticles_slices,{superParticleSymm_slices},clusters2);
[eigenvectors2,~,~,~,Im2] = eigenAnalysis3D(width, clusters2, superParticle_class_fromSP2);


% figure()
% plot(weights(:,1),'o')

%% u2os upper-lower-ring investigation
clc
%superParticle
% z = superParticle{end}(:,3); 
% z = sr(:,3); 
z = superParticleSymm{end}(:,3);
% z = superParticle_class_fromSP2{1}{end}(:,3);

figure()
h1 = histogram(z,100,'Normalization','pdf');
f =fitgmdist(z,2);
xgrid = linspace(-0.6,0.6,1000)';
hold on
plot(xgrid,pdf(f,xgrid),'LineWidth',5)

[~,iUp] = max(f.mu); 
[~,iDown] = min(f.mu);
clc
disp(['lower ' num2str(f.ComponentProportion(iDown))])
disp(['upper ' num2str(f.ComponentProportion(iUp))])

%% Quantify upper-lower per particle 
% close all
mid = 0.03; 

for i = 1:K
%     parZ = extracted{i}{1}(:,3); 
    parZ = subParticles{i}.points(:,3); 
    numLow = sum(parZ<mid); 
    numTotal = size(parZ,1);
    percLow(i) = numLow/numTotal;
    percUp(i) = (numTotal-numLow)/numTotal; 
%     ratio(i) = percLow(i)/percUp(i); 
    ratio(i) = percUp(i)/percLow(i);
end

ratio(ratio==0)=1/1000;  %NOT CORRECT
ratio_log2 = log(ratio)/log(2); 
xtick_positions = (floor(min(ratio_log2))):ceil(max(ratio_log2));
labels = strsplit(num2str(xtick_positions));
labels = cellfun(@(v) insertBefore(v,1,'2^{'),labels,'UniformOutput',false);
labels = cellfun(@(v) [v '}'],labels,'UniformOutput',false);
bins = linspace(min(ratio_log2),max(ratio_log2),40); 

figure()
histogram(ratio_log2(clusters{iUp}),bins)
hold on
histogram(ratio_log2(clusters{iDown}),bins)
% histogram(ratio_log2,bins)
xticks(xtick_positions)
xticklabels(labels)
xlabel('ratio upperRing/lowerRing')
ylabel('count')
title('Ratio number of locs lowerRing over upperRing')
legend('upper','lower')

%% Find Flipping Axis (Improfile)
clc
close all

indx = superParticleSymm{end}(:,3)>0.03; 
upper = superParticleSymm{end}(indx,:);
lower = superParticleSymm{end}(~indx,:);
N=100; 

[imageUp,profileUp] = getCircleImageProfile(upper,N);
[imageLow,profileLow] = getCircleImageProfile(lower,N);

fitSinUp = fit(profileUp.theta',profileUp.c-mean(profileUp.c),'sin1');
fitSinLow = fit(profileLow.theta',profileLow.c-mean(profileLow.c),'sin1');

k = 0:7;   
xUp = pi/2/fitSinUp.b1 +2*pi*k/fitSinUp.b1-fitSinUp.c1/fitSinUp.b1;
xLow = pi/2/fitSinLow.b1 +2*pi*k/fitSinLow.b1-fitSinLow.c1/fitSinLow.b1;

xUpMiddle = xUp(2);     %take the middle point of upper ring
[~,indLow] = min(abs(xLow-xUpMiddle));
angleSymmetry = mean([xUpMiddle,xLow(indLow)])

%plotting (ring images)
figure()
imagesc([imageUp imageLow])
hold on 
plot(profileUp.x,profileUp.y,'r-','LineWidth',3)
hold on 
plot(profileLow.x+N,profileLow.y,'r-','LineWidth',3)
axis equal
axis off
xlim([0 2*N])
ylim([0 N])
title(' Upper - Lower')
hold on
plot([N/2 N/2+cos(angleSymmetry)*N/2], [N/2 N/2+sin(angleSymmetry)*N/2],'r','LineWidth',3)
hold on
plot([N*1.5 N*1.5+cos(angleSymmetry)*N/2], [N/2 N/2+sin(angleSymmetry)*N/2],'r','LineWidth',3)

%plotting (circular Improfiles)
figure()
plot(profileUp.theta,profileUp.c-mean(profileUp.c),'b')
hold on
plot(profileLow.theta,profileLow.c-mean(profileLow.c),'r')
hold on
xx = linspace(0,2*pi,1000); 
plot(xx,fitSinUp(xx),'b')
hold on
plot(xx,fitSinLow(xx),'r')
hold on
plot([angleSymmetry angleSymmetry],2*fitSinLow.a1*[-1 1],'k','LineWidth',3)
grid
grid minor

%plotting (Flip axis)
figure()
imagesc(imageUp+imageLow)
hold on
plot([N/2 N/2+cos(angleSymmetry)*N/2], [N/2 N/2+sin(angleSymmetry)*N/2],'r','LineWidth',3)
axis equal
axis off

%% Flip upper-ring
close all

midZ = mean(f.mu)

classUpper = superParticle_class_fromSP{iUp}{end};       
classLower = superParticle_class_fromSP{iDown}{end};

classLower(:,3) = classLower(:,3)-midZ; 
classUpper(:,3) = classUpper(:,3)-midZ; 

u = [cos(angleSymmetry) sin(angleSymmetry),0];  %axis of rotation
R = rotationAroundArbAxis(u,pi);
upperFlipped = classUpper * R; 

extractedLower = extractFromSuperParticle(subParticles(clusters{iDown}), {classLower}, num2cell(1:length(clusters{iDown})));         
extractedUpper = extractFromSuperParticle(subParticles(clusters{iUp}), {upperFlipped}, num2cell(1:length(clusters{iUp})));         
extractedBoth = [extractedLower extractedUpper];
[i,ii]=sort(vertcat(clusters{[iDown iUp]}));
extractedBoth = extractedBoth(ii); 

for i = 1:K
    extractedBoth2{i}.points = extractedBoth{i}{1};
    extractedBoth2{i}.sigma = subParticles{i}.sigma; 
end


%reBootstrap 
superParticleUpperFlipped_sup = vertcat(extractedBoth{:});
superParticleUpperFlipped_sup = vertcat(superParticleUpperFlipped_sup{:});
[Flipped_sup_reboot, ~] = one2all3D(extractedBoth2, 1, '', '', superParticleUpperFlipped_sup, 1, 1, 1);
temp = extractFromSuperParticle(subParticles,Flipped_sup_reboot(2), num2cell(1:K));   
for i = 1:K
    extractedBoth_reboot{i}.points = temp{i}{end};
    extractedBoth_reboot{i}.sigma = subParticles{i}.sigma; 
end

%plotting surfaces
visualizeCloud3DSurface_Teun(superParticleUpperFlipped_sup,100)
title upperFlipped
visualizeCloud3DSurface_Teun(Flipped_sup_reboot{2},100)
title upperFlippedReboot

% plotting z-histogram
figure()
histogram(superParticleUpperFlipped_sup(:,3),100,'Normalization','pdf')
% hold on
% histogram(Flipped_sup_reboot{2}(:,3),100)
xlabel('Z-coordinate (pixels)')
ylabel('count')
title('Z-distribution before and after flipping upper ring')
legend('before','after')


InteractiveSurface(makeImage3D(superParticleUpperFlipped_sup,50,1),'r');

%plotting surfaces to check flipping
Z1 = makeImage3D(classLower,50,1);
Z2 = makeImage3D(classUpper,50,1);
Z2_flipped = makeImage3D(upperFlipped,50,1); 

[~,p1] = InteractiveSurface(Z1,'g');  title Lower       %lower
[~,p2] = InteractiveSurface(Z2,'r');  title Upper       %upper
[~,p3] = InteractiveSurface(Z2_flipped,'b'); title upperFlipped

fig = figure;
ax = axes; 
copyobj(p1,ax);
copyobj(p3,ax); 
c=camlight('left');
set(c,'style','infinite');
set(gca,'CameraViewAngleMode','manual')
axis equal

%% Reclassify Flipped 

[MatrixFlipped,MatrixFlipped_norm] = MakeMatrixAfterBootstrap3D(extractedBoth_reboot);
% figure()
% imagesc(MatrixFlipped_norm)

% mat = MatrixFlipped_norm;
% distanceMethod = 'complete';
% 
% numclust = 10; 
% threshold = 375; 
% order = 1:K; 
% names = MakeLeafNames(order,threshold); 
% % names = MakeLeafNamesRadius(order,all_shifts);
% 
% clusters = makeTreeAndCluster(mat, distanceMethod, names, numclust,0);
% clusters = clusters( cellfun(@(v) length(v),clusters) >10); 
%  extracted = extractFromSuperParticle(subParticles, Flipped_sup_reboot(2), num2cell(1:K));  	%was Flipped_sup_reboot(2) 4x used     

superParticle_class_fromSPF = extractFromSuperParticle(subParticles,Flipped_sup_reboot(2),clusters2);   

[eigenvectors,eigenvalues,x_mean,weights,Im] = eigenAnalysis3D(width, clusters, superParticle_class_fromSP );

KM = cluster(linkage(weights(:,1),'average'),'maxClust',2); 
clusters1{1} = sort(vertcat(clusters{KM==1}));              %clusters from tree, but merged in 2 
clusters1{2} = sort(vertcat(clusters{KM==2})); 
superParticle_class_fromSP1 = extractFromSuperParticle(subParticles,Flipped_sup_reboot(2),clusters1);   
[eigenvectors1,~,~,~,Im1] = eigenAnalysis3D(width, clusters1, superParticle_class_fromSP1);     %same as eigenvectors for numClust=2

[weights2,KM2,clusters2, ImAll] = projectOnEigenvectors3D(extracted,eigenvectors1,width);
superParticle_class_fromSP2 = extractFromSuperParticle(subParticles,Flipped_sup_reboot(2),clusters2);
[eigenvectors2,~,~,~,Im2] = eigenAnalysis3D(width, clusters2, superParticle_class_fromSP2);

%% CMD
mat = MatrixFlipped_norm;
[cmd] = cmdscale(mat+mat',1);

figure()
plot(cmd(:,1),cmd(:,2),'.')

%% Count bindingsite abundance (see height variation) 

allBindingSites = [];
allOccupances = [];
for i = 1:length(particles)
    allBindingSites = [allBindingSites; particles{i}.binding_sites];
    
    occu = particles{i}.binding_sites_ID; 
    coun = arrayfun(@(x)length(find(occu == x)), unique(occu), 'Uniform', false);
    allOccupances = [allOccupances; cell2mat(coun)];
end

uniqueBindingSites = unique(allBindingSites,'rows');

figure()
scatter3(uniqueBindingSites(:,1),uniqueBindingSites(:,2),uniqueBindingSites(:,3))

%% manually select on height
close all
for i = 231% 1:K
    parZ = extracted{i}{1}(:,3); 
    f =fitgmdist(parZ,2);
    means(i,:) = sort(f.mu)';
    
    figure()
    histogram(parZ,100,'Normalization','pdf')
    xgrid = linspace(-0.6,0.6,1000)';
    hold on
    plot(xgrid,pdf(f,xgrid),'LineWidth',5)

end

figure()
plot(1:K,means(:,1),1:K,means(:,2),1:K,diff(means,1,2))

figure()
scatter(means(:,1),zeros(1,K))

figure()
hist(means(:),1000)
%>does not work

%% Separate on Height
clear parZ parZ_top parZ_top_max subParticles_extr
for i = 1:K
    parZ = particles{i}.binding_sites(:,3); 
    parZ_top = parZ(parZ>0); 
    temp(i) = length(unique(parZ_top));
    parZ_top_max(i) = max(parZ_top);
end

figure()
plot(1:K,parZ_top_mean,'.')
figure()
plot(sort(parZ_top_max))
%> parZ_top_max gives 'height' of simulated particles 
[~,indx] = sort(parZ_top_max); 

for i = 1:K
    subParticles_extr{i}.points = extracted{i}{1};
    subParticles_extr{i}.sigma = subParticles{i}.sigma; 
end

%classify
[MatrixExtracted,MatrixExtracted_norm] = MakeMatrixAfterBootstrap3D(subParticles_extr);

names = MakeLeafNames(indx,375); 
names = MakeLeafNames(1:K,375); 
mat = MatrixAfterBootstrap_remap_norm;
clusters = makeTreeAndCluster(mat, 'complete', names, 10,0);



par = subParticles_slices{174}.points;
figure() 
scatter3(par(:,1),par(:,2),par(:,3),'.')

%% make Tree with ground-truth height-labels
N = 375; 
i = [indx(1:N) indx(end-N+1:end)];
ans = MatrixAfterBootstrap_remap_norm(i,i);
ans = ans+ans';
ans = triu(ans,1); 

names = MakeLeafNames(1:2*N,N);
cc=makeTreeAndCluster(ans,'complete',names,10,0)
clusters = cc; 

figure()
plot(clusters2{1},parZ_top_max(clusters2{1}),'ro','MarkerFaceColor','r')
hold on
plot(clusters2{2},parZ_top_max(clusters2{2}),'bo','MarkerFaceColor','b')

figure()
bins = linspace(min(parZ_top_max),max(parZ_top_max),7);
histogram(parZ_top_max(clusters2{1}),bins)
hold on
histogram(parZ_top_max(clusters2{2}),bins)

clear clusters
clusters{1} = find(parZ_top_max<mean(parZ_top_max));
clusters{1} = temp;
clusters{2} = setdiff(1:K,clusters{1});

%% Z-histograms per class
close all

z = superParticleSymm{end}(:,3);

figure()
h1 = histogram(z(vertcat(clusters{1})),15);
hold on
h2 = histogram(z(vertcat(clusters{2})),15);

%% investigate sigmas 

clear sigmas
sigmas = [];
for i = 1:185
%     sigmas = [sigmas; sqrt(subParticles{i}.sigma)*130];
    sigmas = [sigmas; sqrt(subParticles{i}.sigma(:,1))*130];
end

close all
histogram(sigmas(:,1))

%% Scale Sweep
clear val
clc 

scales = linspace(0.01,0.6,30);
% scales = linspace(0.01,0.5,40);

for t = 1:20
    order = randperm(218);      %use correct N
    idxM = order(1);
    idxS = order(2);  

    M = subParticles{idxM};
    S = subParticles{idxS};
    
    M.points = double(M.points);
    S.points = double(S.points);
    M.sigma = double(M.sigma);
    S.sigma = double(S.sigma); 

    parfor i = 1:length(scales)
        [t,i]
         [~, val(t,i)] = pairFitting3D(M.points, S.points, M.sigma, S.sigma, scales(i), 1, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST,4);
    end
end

val_norm = val./repmat(max(val,[],2),1,length(scales)); 

figure()
for i = 1:size(val,1)
    plot(scales,val_norm(i,:))
    hold on
end
hold on
plot(scales,mean(val_norm,1),'k','LineWidth',5)
xlabel('scale (pixels)')
ylabel('costValue (norm. per line)')
title('Scale Sweep')
grid; grid minor




