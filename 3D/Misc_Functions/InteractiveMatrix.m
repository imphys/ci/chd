function InteractiveMatrix(Z)

f = figure; 
ax = axes('Parent',f,'position',[0.13 0.25  0.7 0.70]);

% ax = axes('Parent',f,'position',[0.13 0.39  0.77 0.54]);
% i = floor(size(Z,1)/2); 
i = floor(size(Z,1)/2); 
p1 = imagesc(Z(:,:,i)); 
axis off
axis square
colormap hot
caxis([0 max(Z(:))])

b = uicontrol('Parent',f,'Style','slider','Position',[81,54,419,23],...
              'value',i, 'min',1, 'max',size(Z,1));
bgcolor = f.Color;
bl1 = uicontrol('Parent',f,'Style','text','Position',[50,54,23,23],...
                'String','0','BackgroundColor',bgcolor);
bl2 = uicontrol('Parent',f,'Style','text','Position',[500,54,23,23],...
                'String','1','BackgroundColor',bgcolor);
bl3 = uicontrol('Parent',f,'Style','text','Position',[190,25,200,23],...
                'String',['z = ' num2str(round(i))],'BackgroundColor',bgcolor);
            
b.Callback = {@callback,Z,bl3};
% addlistener(b,'Value',@callback)

function callback(h,event,Z,bl3)
        imagesc(Z(:,:,(round(h.Value))))
        axis square
        colormap hot
        caxis([min(Z(:)) max(Z(:))])
        bl3.String = ['z = ' num2str(round(h.Value))];
end

end
