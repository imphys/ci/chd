function [AverageD,Dmatrix,Dmatrix1]=particleDistAverageModels(TBindingSites)
%Average distance between bindingsites of asymmetric models
%Input
%TBindingSites: Transformed binding sites of particles
%Output
%Average D, Average distance between binding sites
%Dmatrix
D=0;
number=size(TBindingSites,1);
totalNumber=number*(number-1)/2;
iter=0;
for i=1:1:size(TBindingSites,1)-1
    par1=TBindingSites{i,1};
    for j=i+1:1:size(TBindingSites,1)
        iter=iter+1;
        par2=TBindingSites{j,1};
        Dmatrix(i,j)=particleDistModels(par1, par2);
        D =D + particleDistModels(par1, par2);
        Dmatrix1(iter)=Dmatrix(i,j)/totalNumber;
    end
end
Nc=size(par1,1);
AverageD=D/Nc/totalNumber;