%Combine particles into one super particles
function [superParticle] = CombineParticles(Particles)
superParticle=[];
Nparticles=size(Particles,2);
Sigma=[];
Coords=[];
Points=[];
for i=1:1:Nparticles
Particle_Add=Particles{1,i};
Sigma=[Sigma;Particle_Add.sigma];
Coords=[Coords;Particle_Add.coords];
VP= Particle_Add.coords(:,1:2);%'*CCD_pixelsize;
Points=[Points;VP];
end
superParticle.sigma=Sigma;
superParticle.coords=Coords;
superParticle.points=Points;
end

