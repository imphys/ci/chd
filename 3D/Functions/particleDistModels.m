function D = particleDistModels(par1, par2)
Sort=[1:1:size(par1,2)];
for i=1:1:3
    [Nump1,p1]= sort(par1(i,:));
    tmpPar1=par1(:,p1);
    [Nump2,p2]= sort(par2(i,:));
    tmpPar2=par2(:,p2);
    Dist(i)= sum(sqrt(sum((tmpPar1 - tmpPar2).^2, 2)));
    Sorttmp=[p1;p2];
    [NumpS,pS]= sort(Sorttmp(1,:));
    Sort=[Sort;p2(pS)];
end
SortFinal=[1:1:size(par1,2)];
index_accumulate=[];
for j=1:1:size(par1,2)
    [M,F]=mode(Sort(2:1:4,j));
    if F>1
        SortFinal(2,j)=M;
        index_accumulate=[index_accumulate,M];
    else
        SortFinal(2,j)=0;
    end
end
IndexAll=[1:1:size(par1,2)];
AddPosition=find(SortFinal(2,:)==0);
while size(AddPosition,2)>0
    AddPositionNew=find(SortFinal(2,:)==0);
    
    
    for k= AddPosition
        Rest_index=Sort(2:1:4,k);
        Index_common=intersect(index_accumulate,Rest_index);
        if size(Index_common,1)>0
            
            for kk=1:1:size(Index_common,2)
                
                deleteposition=find(Rest_index== Index_common(kk));
                if size(deleteposition,2)>0
                    Rest_index(deleteposition)=[];
                end
            end
            
            if size(Rest_index,1)==1
                SortFinal(2,k)=Rest_index;
                index_accumulate=[index_accumulate, SortFinal(2,k)];
            end
        end
    end
    AddPosition=find(SortFinal(2,:)==0);
    if size(AddPosition,2)==size(AddPositionNew, 2)
        break
    end
end

if size(AddPosition,2)==0
    position2=SortFinal(2,:);
    tmpPar3=par2(:,position2);
    Dist(4)= sum(sqrt(sum((par1 - tmpPar3).^2, 2)));
else
    
    for k= AddPosition
        SortFinal(2,k)=0;
    end
    deleteposition1=find( SortFinal(2,:)== 0);
    if size(deleteposition1,2)>0
        SortFinal(:, deleteposition1)=[];
    end
    position1=SortFinal(1,:);
    Par1A=par1(:,position1);
    position2=SortFinal(2,:);
    Par2A=par2(:,position2);
    DistPart1= sum(sqrt(sum((Par1A - Par2A).^2, 2)));
    
    
    par3=par1(:,AddPosition);
    restPosition=[1:1:size(par1,2)];
    restPosition(position2)=[];
    par4=par2(:,restPosition);
    for i=1:1:3
        [Nump3,p3]= sort(par3(i,:));
        tmpPar3=par3(:,p3);
        [Nump4,p4]= sort(par4(i,:));
        tmpPar4=par4(:,p4);
        DistPart2A(i)= sum(sqrt(sum((tmpPar3 - tmpPar4).^2, 2)));
    end
    DistPart2=min(DistPart2A);
    Dist(4)=DistPart2+DistPart1;
end


D =min(Dist);


end