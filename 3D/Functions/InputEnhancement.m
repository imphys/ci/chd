function [ParticlesEnhancement,Ven] = InputEnhancement(Particles,V,Copytime)
ParticleNumber=size(Particles,2);

Ven={};

N=ParticleNumber;

for i=1:N
    ParticlesEnhancement{1,i}.sigma=repmat(Particles{1,i}.sigma,Copytime,1);
    ParticlesEnhancement{1,i}.points=repmat(Particles{1,i}.points,Copytime,1);
    Ven{i,1}=repmat(V{i,1},1,Copytime);
end
end

