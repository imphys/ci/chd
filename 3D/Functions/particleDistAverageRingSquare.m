function [AverageD,Dmatrix,Dmatrix1]=particleDistAverageRingSquare(TBindingSites)
D=0;
iter=0;
number=size(TBindingSites,1);
Dmatrix=zeros(number,number);
for i=1:1:size(TBindingSites,1)-1
    par1=TBindingSites{i,1}';
    for j=i+1:1:size(TBindingSites,1)
        iter=iter+1;
        par2=TBindingSites{j,1}';
        Dmatrix(i,j) = particleDistRingSquare(par1, par2);
        D =D + Dmatrix(i,j);
        Dmatrix1(iter)=Dmatrix(i,j)/16;
    end
    
end
totalNumber=number*(number-1)/2;
AverageD=D/16/totalNumber;
end