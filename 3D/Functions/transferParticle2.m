%transfer particles between clusters
%[TVtrans1,TBtrans1]=transferParticle(output1,common1,TVforward,TBindingSitesforward,Rforward,tforward,Rturn,tturn);

function [TP,TB]=transferParticle2(OriginPar,OriginBindingSites,Rback,tback,Rtransfer,ttransfer)
for i=1:1:size(OriginPar,1)
    Mback{i,1}=Rback'*(OriginPar{i,1}-tback);
    TBback{i,1}=Rback'*(OriginBindingSites{i,1}-tback);
    TP{i,1}=Rtransfer*Mback{i,1}+ttransfer;
    TB{i,1}=Rtransfer*TBback{i,1}+ttransfer;
end
end