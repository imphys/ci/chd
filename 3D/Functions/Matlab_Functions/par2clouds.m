function [cloud]=par2clouds(particles,range)
cloudp=[];
clouds=[];

for i=range
  clouds=[clouds;particles{1,i}.sigma];
cloudp=[cloudp;particles{1,i}.points];

end
cloud.points=cloudp;
 cloud.sigma=clouds;