%% Scale sweep

addpath(genpath('2D_All2All'))
addpath(genpath('Data'))

dataset = 'Emory';           %100 with flame, 100 without flame (80% DoL)
% -- choose number of particles --
load(['data/2D/' dataset '/20220117_DTR_ATPdep_4h_(+)Ant.A_(+)N.mat'])
 N = length(particles);
% 
CCD=130;
for i=1:N
    subParticles{1,i}.points=(subParticles{1,i}.points/CCD-mean(subParticles{1,i}.points/CCD));
    subParticles{1,i}.sigma=((subParticles{1,i}.sigma/100));
end

 subParticles=subParticles_cropped_Top;
% clear val

scales = linspace(0.001,2,100);
% scales = linspace(0.01,0.5,40);

for t = 1:20
    order = randperm(N);  %shuffle all particles (change 372 to number of particles)
    idxM = order(1);
    idxS = order(2);  

    M = subParticles{idxM}
    S = subParticles{idxS}
    
    M.points = double(M.points);
    S.points = double(S.points);
    M.sigma = double(M.sigma);
    S.sigma = double(S.sigma); 

    parfor i = 1:length(scales)
        [t,i]
        [~, ~, ~, ~, val(t,i)] = pairFitting(M, S, scales(i),6);
    end
end

val_norm = val./repmat(max(val,[],2),1,length(scales)); 

figure()
for i = 1:size(val,1)
    plot(scales,val_norm(i,:))
    hold on
end
hold on
plot(scales,mean(val_norm,1),'k','LineWidth',5)
xlabel('scale (pixels)')
ylabel('costValue (norm. per line)')
title('Scale Sweep')
grid; grid minor