%particles should contain points and sigma like {1,N} Where N is the total
%number of the particles
%In each cell there should be double points like loc*2
%In each cell there should be double sigma like loc*1

function vis2(particles,range,anim_time)
for i=range
    scatter(particles{1,i}.points(:,1),particles{1,i}.points(:,2),1,'.')
    view
    hold on
    pause(anim_time)
end
end