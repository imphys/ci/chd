function [ registered_chunk TV_All]=function_JRMPC3D(Particles)

close all
 addpath(genpath('3D_JRMPC'))

nc1set=[1]    ;            %%%%%%%%%%number of clusters classified for all particles

cluster_mode=2;          %1: H,2:MDS
Repeattimes=10;           %Number of independent JCC progress for a single NUP model
limit=0;

CCD_pixelsize=130;

Particles=Particles(~cellfun('isempty',Particles));


ParticleNumber=size(Particles,2);
N=ParticleNumber;
minLo=0;
maxLo=200000;%maximumlocalization in one particle
[Particles1,V1,meanlocs,points_a,sigma_a] = ProcessedExperimentalDataPraticle(Particles,CCD_pixelsize,N,minLo,maxLo);

%  Particles1=cellfun(@(Particles,Identity)  V(1,Identity),Particles,Identity,'uniformoutpupoints1=cellfun(@(V,Identity)  V(:,Identity),points_a,Identity,'uniformoutput',false);t',false);
N1=size(V1,1);
raster_spacing=50;       % expected resolution [nm]
tic


% K = K_estimate(V1,meanlocs,20,0.5*raster_spacing) %Estimate number of Gaussian models

K=5;
%------------------prepare output format for parallel for------------------%
R_All=cell(Repeattimes,1);              %rotation matrix
t_All=cell(Repeattimes,1);               %translation matrix
TV_All=cell(Repeattimes,1);            %Transformed particles
TB_All=cell(Repeattimes,1); %Transformed particles after removing outliers
X_All=cell(Repeattimes,1);
X_inAll=cell(Repeattimes,1);
%------------------prepare output format for parallel for------------------%

parfor JK=1:Repeattimes                                %%% this one was parfor aswell
    %estimate number of GMMcenters K for JRMPC
    %------------------Generate Xin1-----------------
    az1 = 2*pi*rand(1,K);
    el1 = 2*pi*rand(1,K);
    Xin1 = [cos(az1).*cos(el1); sin(el1); sin(az1).*cos(el1)];
    Xin1 = Xin1*100;%CCD_pixelsize;
    %------------------Generate Xin1-----------------
    
    %------------------JRMPC 2D-----------------
    %[R ,t,X,S,a] = jrmpc(V1,Xin1,'maxNumIter',100, 'epsilon', 1e-5,'gamma',0.2);   %JRMPC
    [R ,t,X,S,a] = jrmpc_Nooutliers(V1,Xin1,'maxNumIter',500, 'epsilon', 1e-5);%,'S',Sa)%'gamma',0.2);%'S',1000);   %JRMPC
    
    TV = cellfun(@(V,R,t) bsxfun(@plus,R*V,t),V1,R,t ,'uniformoutput',false); %Transformed V
    [TB,Xrefined,Xrem] = removePointsAndCenters(TV,X,S,a);           %Registration after removing extra centers and noise
    %------------------JRMPC 2D-----------------
    R_All{JK,1}=R;                        %all Rotation ma                        az1 = 2*pi*rand(1,K);
    t_All{JK,1}=t;                        %all translation matrix from PMKth JRMPC
    TV_All{JK,1}=TV;                      %all transformed particles from PMKth JRMPC
    TB_All{JK,1}= TB ;
    X_All{JK,1}=X;
    X_inAll{JK,1}=Xin1;
    S_All{JK,1}=S;
    
    %------------------JRMPC 2D-----------------
end
disp('JRMPC parfor finished');
toc
Time_JRMPC=toc;


%...-----------------Calculate Cost Function-----------
cost_norm_All=cell(Repeattimes,1);
parfor J_C1=1:Repeattimes%%%%%%%%%%%%%%                %%was parfor%%%%%%%%%%%%%%%5 *******************
    TV_p=TV_All{ J_C1,1};
    R_p=R_All{ J_C1,1};
    t_p=t_All{ J_C1,1};
    [~, MatrixAfterAll2all_norm]=Cost_3DReal(V1,TV_p,R_p,t_p,Particles1,CCD_pixelsize);
    cost_norm_All{ J_C1,1}=MatrixAfterAll2all_norm;
end
%----------------Calculate Cost Function-----------

%----------------Classify all clusters-----------

clus_All=cell(Repeattimes,1);
ClusterView1_All=cell(Repeattimes,1);
clusfull_All=cell(Repeattimes,1);
for nc1=nc1set
    tic
    minClustSize=N1/nc1/2; %%%%%%%%%%%minimum particles included in one ''good'' cluster
    for J_C1=1:Repeattimes%%%%%%%%%%%%%%
        TV_p=TV_All{ J_C1,1};
        MatrixAfterAll2all_norm=  cost_norm_All{ J_C1,1};
        %  [~, MatrixAfterAll2all_norm]=Cost_2D(TV_p,Particles1,CCD_pixelsize);
        clusterfull=[];
        if cluster_mode==1
            [~,clusterfull]=H_Clustering(MatrixAfterAll2all_norm,nc1,minClustSize);
        else
            [~,clusterfull]=MDS_Clustering3D(MatrixAfterAll2all_norm,nc1,minClustSize);% First Classify
        end
        clusfull_All{ J_C1,1}=clusterfull;
    end
    toc
    Time_Classify=toc;
    
    %----------------Select Good clusters----------
    
    %  clus= clusterfull( cellfun(@(v) length(v),clusterfull) >= minClustSize);
    
    for J_C1=1:Repeattimes%%%%%%%%%%%%%%%%%%%%
        clusterfull=clusfull_All{ J_C1,1};
        clus= clusterfull( cellfun(@(v) length(v),clusterfull) >= minClustSize);
        
        if size(clus,2)==0
            ClusterView=[0,0]
            clus=0;
        elseif size(clus,2)==1
            ClusterView=[J_C1,1,size(clus,1)];
        else
            for ic=1:1:size(clus,2)
                ClusterView(ic,:)=[J_C1,ic,size(clus{1,ic},1)];
            end
        end
        
        
        clus_All{ J_C1,1}=clus;
        ClusterView1_All{ J_C1,1}=ClusterView;
    end
    
end
%----------------Select Good clusters----------
%----------------Classify all clusters----------

toc
Time_Select=toc;
tic
%------------------Connect------------------
ClusterViewMatrix=cell2mat(ClusterView1_All);
[maxrow,maxrowposition]=max(ClusterViewMatrix(:,3));
maxR=ClusterViewMatrix(maxrowposition,1);
maxC=ClusterViewMatrix(maxrowposition,2);
clus_initial=clus_All{maxR,1}{1,maxC};                   %%%%%%%%%%%%%main cluster to connect all possible clusters
[TVPick, ReorderParticles, Rpick,tpick] = ConnectParticlesInDifferentClusterWithSigma(limit,Repeattimes,clus_All,clus_initial,maxR,R_All,t_All,TV_All,Particles1);
%------------------Connect------------------
disp('Connected finished');
toc
Time_Connect=toc;
TimeCC=Time_Connect+Time_Classify+Time_Select;  TimeAll=TimeCC+Time_JRMPC;
time=TimeAll
effectiveParticlesNumber=size(TVPick,1);%number of particles included in the final results.

for i=1:length(TVPick)
    registered_chunk{1,i}.points= TVPick{i,1}';
    registered_chunk{1,i}.sigma= ReorderParticles{i,1}.sigma;
end

end