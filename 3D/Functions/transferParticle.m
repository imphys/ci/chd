%transfer particles between clusters
 %[TVtrans1,TBtrans1]=transferParticle(output1,common1,TVforward,TBindingSitesforward,Rforward,tforward,Rturn,tturn);
            
 function [Mforth,TBforth]=transferParticle(TransferredParticleID,commonID,TVturn,TBindingSitesturn,Rturn,tturn,Rforth,tforth)
output=TransferredParticleID;
Mforth={};
TBforth={};
for pi=output'
            Mfinal=TVturn{pi,1};
            TBfinal=TBindingSitesturn{pi,1};
            Mback=Rturn{commonID,1}'*(Mfinal-tturn{commonID,1});
            TBback=Rturn{commonID,1}'*(TBfinal-tturn{commonID,1});
            Mforth1=Rforth{commonID,1}*Mback+tforth{commonID,1};
            TBforth1=Rforth{commonID,1}*TBback+tforth{commonID,1};
            Mforth=[Mforth;Mforth1];
            TBforth=[TBforth;TBforth1];
end
 end