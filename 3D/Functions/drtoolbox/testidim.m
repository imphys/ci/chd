X=mds     
n = size(X, 1);
            D = find_nn(X, 5);
            [foo, bar, val] = find(D);
            r1 = median(val); r2 = max(val);
            s1 = 0; s2 = 0;
            X = X';
            XX = sum(X .^ 2);
            onez = ones(1,n);
            for i=1:n         
                p = X(:,i)';
                xx = sum(XX(:,i));
                xX = p * X;
                dist = xx * onez + XX - 2 * xX;
                dist = sqrt(dist(i+1:n));
                s1 = s1 + length(find(dist < r1));
                s2 = s2 + length(find(dist < r2));
            end
            Cr1 = (2 / (n * (n - 1))) * s1;
            Cr2 = (2 / (n * (n - 1))) * s2;

            % Estimate intrinsic dimensionality
            no_dims = (log(Cr2) - log(Cr1)) / (log(r2) - log(r1));
            