function D = particleDistRingSquare(par1, par2)

    Dist = zeros(1,16);
    chunk1 = par2(1:8,:);      % upper ring 1 set
    chunk2 = par2(9:16,:);     % upper ring, shifted by 10 deg to make dubble blob per set
    

    for i=1:8
        tmpPar2 = [circshift(chunk1, i-1);
                            circshift(chunk2, i-1)];
                           Dist(i)=sum(sqrt(sum((par1 - tmpPar2).^2, 2)));
        
    end
    
    %supplement from Wenxiu
    chunk11=flip(chunk1);
    chunk21=flip(chunk2);
 
    % % suggestion from bernd for uppper lower flipping
    for i=9:16
        tmpPar2 = [circshift(chunk11, i+1);
                           circshift(chunk21, i+1)];
                          
      Dist(i)=sum(sqrt(sum((par1 - tmpPar2).^2, 2)));
    end

    D = min(Dist);

end