function K =Centers_estimate(X,bandwidth)

%Use meanshift to get the distribution of centers
dataPts=X';
[clustCent,data2cluster,cluster2dataCell] = MeanShiftCluster(dataPts,bandwidth);

VS = cellfun(@(V) size(V,2),cluster2dataCell,'uniformoutput',false);
VSM=cell2mat(VS);
limit=15;
position_large=find(VSM>limit);
Nl=size(position_large,1);
if Nl>0
    for i=1:Nl
        p=position_large(i);
        K_final(:,i)=clustCent(:,p);
    end
    
    K=size(K_final,2);
else
    K=1;
end