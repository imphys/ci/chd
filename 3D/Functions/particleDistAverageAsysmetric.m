function [AverageD,Dmatrix,Dmatrix1]=particleDistAverage2DAsysmetric(TBindingSites)
D=0;
iter=0;
number=size(TBindingSites,1);
Dmatrix=zeros(number,number);
totalNumber=number*(number-1)/2;
Nbindingsites=size(TBindingSites{1,1}',1);
for i=1:1:size(TBindingSites,1)-1
    par1=TBindingSites{i,1}';
    for j=i+1:1:size(TBindingSites,1)
        iter=iter+1;
        par2=TBindingSites{j,1}';%Np*2
        
        Dmatrix(i,j) = sum(sqrt(sum((par1 - par2).^2, 2)));
        D =D + Dmatrix(i,j);
        Dmatrix1(iter)=Dmatrix(i,j)./Nbindingsites;
    end
    
end

AverageD=D/Nbindingsites/totalNumber;

end
