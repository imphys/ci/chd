function [clusback]=poorclusters_reselect(raster_spacing,clus,TV,sizeview,seeClusters)
bandwidth_c=raster_spacing*0.5;
NumberofCenters=[];
for i=1:size(clus,2)
    TVS=[];
    TVS=TV(clus{1,i});
    
    ParticlesNumberIncluster=size(clus{1,i},1);
    data2c=[];
    data2c=cell2mat(TVS')';
    idx = find(data2c(:,1) < sizeview & ...
        data2c(:,1) > -sizeview & ...
        data2c(:,2) <  sizeview  & ...
        data2c(:,2) > -sizeview & ...
        data2c(:,3) <  sizeview & ...
        data2c(:,3) > -sizeview);
    data2c = data2c(idx,:);
    NumberofCenters(i) =Centers_estimate(data2c,bandwidth_c);
    c=NumberofCenters(i);
   
    if seeClusters==1
        figure()
        hold on
        densityc=visualizeCloud3D2(data2c,10,0);
        scatter3(data2c(:,1),data2c(:,2), data2c(:,3),1,densityc,'.');
        title(sprintf('Number of particles=%d,centers=%d',ParticlesNumberIncluster,c),'fontweight','bold','fontsize',12,'Color', 'w');
        colormap(hot);
        colordef black
        %set(gcf,'Color','w');
        %set(gca, 'FontSize', 16,'FontWeight','bold')
        %axis equal, axis square
        hold off
    end
end
boundarynumber=min(NumberofCenters)*1.5;
    idx2 = find(NumberofCenters <boundarynumber) ;
    if size(idx2,2)==1
        clusback{1,1}=clus{1,idx2};
    else
     for i=1:size(idx2,2)
    clusback{1,i}= clus{1,idx2(1,i)};
     end
    end

end