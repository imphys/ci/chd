clc
clear
close all
path_mex_matlab1 = genpath('build/mex/');
path_mex_matlab2 = genpath('build/figtree/src/mex/');
path_matlab = genpath('MATLAB');
path_function=genpath('Functions');
path_ExperimentalData=genpath('ExperimentalData');
path_functionTeun=genpath('TeunsFunctions');
addpath(path_mex_matlab1)
addpath(path_mex_matlab2)
addpath(path_matlab)
addpath(path_function)
addpath(path_ExperimentalData)
addpath(path_functionTeun)
ifinal=0;
% CPU/GPU settings (CPU = 0, GPU = 1)
USE_GPU_GAUSSTRANSFORM = 1;
USE_GPU_EXPDIST = 1;
nc1set=[30]

Boundary=1.5;            % This is Boudary to remove outliers. We did not use this at here
CCD_pixelsize=130;
cluster_mode=1;%1: H,2:MDS
Repeattimes=2;           %Number of independent JCC progress for a single NUP model
sizeview=150;
seeClusters=0;
poordata=1;
limit=0;
ifinal=0;%size(Final,1);
typeset=[4];
for type=typeset
    Particles=[];
    if type==1
        load('3D_NUPs.mat');
        %x,y need CCD pixels
        dol=0.5;
        
    end
    if type==2
        load('RawData_306_NUP.mat');
        %this is of the same formal with experimental data
        dol=0.35;
        model_name='RawData_306_NUP';
    end
    if type==3
        load('RawData_750_NUP.mat');
        %Sigma =1 and don't need CCD_pixels
        dol=0.35;
    end
        if type==4
        load('NUP107_PAINT_306x_RalfHamid.mat');
        %Sigma =1 and don't need CCD_pixels
        dol=0.35;
        Particles=NUP107PAINTParticles;
    end
    ParticleNumber=size(Particles,2);
    N=ParticleNumber;
    Localizationlimit=0;
    [Particles0,V0,meanlocs0] = ProcessedExperimentalDataPraticle(Particles,CCD_pixelsize,N,Localizationlimit);
    for i=1:N
        Positive=find(Particles{1,i}.points(:,3)>0);
        Negative=find(Particles{1,i}.points(:,3)<=0);
        ParticlesPositive{1,i}.sigma=double(Particles{1,i}.sigma(Positive,1));
        ParticlesPositive{1,i}.points=double(Particles{1,i}.points(Positive,1:2));
      %  V0Positive{i,1}=ParticlesPositive{1,i}.points'.*CCD_pixelsize;
              ParticlesNegative{1,i}.sigma=double(Particles{1,i}.sigma(Negative,1));
        ParticlesNegative{1,i}.points=double(Particles{1,i}.points(Negative,1:2));
     %   V0Negative{i,1}=ParticlesNegative{1,i}.points'.*CCD_pixelsize;
    end
end
