function [MatrixAfterAll2all, MatrixAfterAll2all_norm] = MakeMatrix3Dn(Values,subParticles)

K = length(subParticles);
Mat = zeros(K,K);
Mat_norm = zeros(K,K);

numLocs = cellfun(@(v) size(v.points,1),subParticles);

for i = 1:length(Values)
    for j=i:length(Values)
    Mat(i,j) = Values(i,j); 
    Mat_norm(i,j) = Values(i,j)/(numLocs(i)*numLocs(j));
    end
end

MatrixAfterAll2all = Mat; 
MatrixAfterAll2all_norm = Mat_norm; 


end

