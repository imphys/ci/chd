
addpath(genpath('Data'))

%% Part2subpart
%     for j=1:10
%        subParticles{j}.points = particles{1,j}.coords(:,1:2);
%        subParticles{j}.sigma  = particles{1,j}.coords(:,5).^2;
%     end

%% test all2all2D
%export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/MATLAB/R2019a/runtime/glnxa64:/usr/local/MATLAB/R2019a/bin/glnxa64:/usr/local/MATLAB/R2019a/sys/os/glnxa64:/usr/local/MATLAB/R2019a/sys/opengl/lib/glnxa64:/home/shaghparast/ISO_PCA_NEW/2D_All2All/build/mex
% addpath(genpath('2D_All2All'))
% dataset = 'NUPsim_test_10';           %100 with flame, 100 without flame (80% DoL)
% % -- choose number of particles --
% N = 200;     %length(subparticles)
% load(['data/2D/' dataset '/subParticles.mat'])
% outdir = ['output/' dataset];
% if ~exist(outdir,'dir')
%     mkdir(outdir);
% else
%     disp('ERROR: outdir already exists')
% end    
% save([outdir '/subParticles'], 'subParticles');
% scale = 0.01;   
% %all2all registration
% all2all(subParticles, [outdir '/all2all_matrix'], scale);           
% %optional fusion of all particles (not necessary for classification)
% [initAlignedParticles, M1] = outlier_removal(subParticles, [outdir '/all2all_matrix/'], outdir);        %Lie-algebraic averaging


%% test all2all3D  (All functions works but tuning study needed)
%export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/MATLAB/R2019a/runtime/glnxa64:/usr/local/MATLAB/R2019a/bin/glnxa64:/usr/local/MATLAB/R2019a/sys/os/glnxa64:/usr/local/MATLAB/R2019a/sys/opengl/lib/glnxa64:/home/shaghparast/ISO_PCA_NEW/3D_All2All/build/mex
addpath(genpath('3D_All2All'))


load('Tetra218_for_test_3D.mat')
N=length(subParticles);
USE_GPU_GAUSSTRANSFORM = 1;
USE_GPU_EXPDIST = 1;
initAng = 'grid_72.qua';
scale = 0.2;   
nIterations = 5;           % number of lie-algebra avg and consistency check                           % iterations
flagVisualizeSijHist = 0;   % show S_ij histogram (boolean) 
threshold = 0.5;            % consistency check threshold.
[RR, I] = all2all3D(subParticles, scale, initAng, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST);
[initAlignedParticles, sup] = relative2absolute(subParticles, RR, I, N, nIterations, threshold, 1);
for i=1:N
    scatter3(initAlignedParticles{1,i}.points(:,1),initAlignedParticles{1,i}.points(:,2),initAlignedParticles{1,i}.points(:,3),1,'.')
hold on
end

%%
%New 3D JRMPC+Classify+Connect
%close all
%first mex
%export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:MATLAB_DIRECTORY/runtime/glnxa64:MATLAB_DIRECTORY/bin/glnxa64:MATLAB_DIRECTORY/sys/os/glnxa64:MATLAB_DIRECTORY/sys/opengl/lib/glnxa64:BUILD_DIRECTORY/mex
%export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/ud/Matlab-R2018a/extern/bin/glnxa64:/opt/ud/Matlab-R2018a/runtime/glnxa64:/opt/ud/Matlab-R2018a/sys/os/glnxa64:/opt/ud/Matlab-R2018a/sys/opengl/lib/glnxa64:/home/shaghparast/ISO_PCA_NEW/3D_JRMPC/build/mex:/home/shaghparast/ISO_PCA_NEW/3D_JRMPC/build/figtree/src/mex:/home/shaghparast/ISO_PCA_NEW/3D_JRMPC/build/figtree/external/ann_1.1.1



%  addpath(genpath('3D_JRMPC'))
% 
% ifinal=0;
% % CPU/GPU settings (CPU = 0, GPU = 1)
% USE_GPU_GAUSSTRANSFORM = 1;
% USE_GPU_EXPDIST = 1;
% 
% nc1set=[2]                %%%%%%%%%%number of clusters classified for all particles
% 
% Boundary=1.5;            % This is Boudary to remove outliers. We did not use this at here
% 
% cluster_mode=2;          %1: H,2:MDS
% Repeattimes=3;           %Number of independent JCC progress for a single NUP model
% sizeview=100;
% limit=0;
% ifinal=0;%size(Final,1);
% %Sa=800
% typeset=4; %5 is tetra datasets
% 
%  load('Tetra218_for_test_3D.mat')
% %Sigma =1 and don't need CCD_pixels
% dol=0.35;
% Particles=subParticles;
% model_name='tetra'
% CCD_pixelsize=160;
% 
% 
% 
% ParticleNumber=size(Particles,2);
% N=ParticleNumber;
% minLo=0;
% maxLo=10000;%maximumlocalization in one particle
% [Particles1,V1,meanlocs,points_a,sigma_a] = ProcessedExperimentalDataPraticle(Particles,CCD_pixelsize,N,minLo,maxLo);
% 
% % Particles1=cellfun(@(Particles,Identity)  V(1,Identity),Particles,Identity,'uniformoutpupoints1=cellfun(@(V,Identity)  V(:,Identity),points_a,Identity,'uniformoutput',false);t',false);
% N1=size(V1,1);
% raster_spacing=50;       % expected resolution [nm]
% tic
% 
% 
% % K = K_estimate(V1,meanlocs,20,0.5*raster_spacing) %Estimate number of Gaussian models
% 
% K=9
% %------------------prepare output format for parallel for------------------%
% R_All=cell(Repeattimes,1);              %rotation matrix
% t_All=cell(Repeattimes,1);               %translation matrix
% TV_All=cell(Repeattimes,1);            %Transformed particles
% TB_All=cell(Repeattimes,1); %Transformed particles after removing outliers
% X_All=cell(Repeattimes,1);
% X_inAll=cell(Repeattimes,1);
% %------------------prepare output format for parallel for------------------%
% 
% parfor JK=1:Repeattimes
%     %estimate number of GMMcenters K for JRMPC
%     %------------------Generate Xin1-----------------
%     az1 = 2*pi*rand(1,K);
%     el1 = 2*pi*rand(1,K);
%     Xin1 = [cos(az1).*cos(el1); sin(el1); sin(az1).*cos(el1)];
%     Xin1 = Xin1*100;%CCD_pixelsize;
%     %------------------Generate Xin1-----------------
%     
%     %------------------JRMPC 2D-----------------
%     %[R ,t,X,S,a] = jrmpc(V1,Xin1,'maxNumIter',100, 'epsilon', 1e-5,'gamma',0.2);   %JRMPC
%     [R ,t,X,S,a] = jrmpc_Nooutliers(V1,Xin1,'maxNumIter',500, 'epsilon', 1e-5);%,'S',Sa)%'gamma',0.2);%'S',1000);   %JRMPC
%     
%     TV = cellfun(@(V,R,t) bsxfun(@plus,R*V,t),V1,R,t ,'uniformoutput',false); %Transformed V
%     [TB,Xrefined,Xrem] = removePointsAndCenters(TV,X,S,a);           %Registration after removing extra centers and noise
%     %------------------JRMPC 2D-----------------
%     R_All{JK,1}=R;                        %all Rotation ma                        az1 = 2*pi*rand(1,K);
%     t_All{JK,1}=t;                        %all translation matrix from PMKth JRMPC
%     TV_All{JK,1}=TV;                      %all transformed particles from PMKth JRMPC
%     TB_All{JK,1}= TB ;
%     X_All{JK,1}=X;
%     X_inAll{JK,1}=Xin1;
%     S_All{JK,1}=S;
%     
%     %------------------JRMPC 2D-----------------
% end
% disp('JRMPC parfor finished');
% toc
% Time_JRMPC=toc;
% 
% 
% %...-----------------Calculate Cost Function-----------
% cost_norm_All=cell(Repeattimes,1);
% parfor J_C1=1:Repeattimes%%%%%%%%%%%%%%
%     TV_p=TV_All{ J_C1,1};
%     R_p=R_All{ J_C1,1};
%     t_p=t_All{ J_C1,1};
%     [~, MatrixAfterAll2all_norm]=Cost_3DReal(V1,TV_p,R_p,t_p,Particles1,CCD_pixelsize);
%     cost_norm_All{ J_C1,1}=MatrixAfterAll2all_norm;
% end
% %----------------Calculate Cost Function-----------
% 
% %----------------Classify all clusters-----------
% 
% clus_All=cell(Repeattimes,1);
% ClusterView1_All=cell(Repeattimes,1);
% clusfull_All=cell(Repeattimes,1);
% for nc1=nc1set
%     tic
%     minClustSize=N1/nc1/2; %%%%%%%%%%%minimum particles included in one ''good'' cluster
%     for J_C1=1:Repeattimes%%%%%%%%%%%%%%
%         TV_p=TV_All{ J_C1,1};
%         MatrixAfterAll2all_norm=  cost_norm_All{ J_C1,1};
%         %  [~, MatrixAfterAll2all_norm]=Cost_2D(TV_p,Particles1,CCD_pixelsize);
%         clusterfull=[];
%         if cluster_mode==1
%             [~,clusterfull]=H_Clustering(MatrixAfterAll2all_norm,nc1,minClustSize);
%         else
%             [~,clusterfull]=MDS_Clustering3D(MatrixAfterAll2all_norm,nc1,minClustSize);% First Classify
%         end
%         clusfull_All{ J_C1,1}=clusterfull;
%     end
%     toc
%     Time_Classify=toc;
%     
%     %----------------Select Good clusters----------
%     
%     %  clus= clusterfull( cellfun(@(v) length(v),clusterfull) >= minClustSize);
%     
%     for J_C1=1:Repeattimes%%%%%%%%%%%%%%%%%%%%
%         clusterfull=clusfull_All{ J_C1,1};
%         clus= clusterfull( cellfun(@(v) length(v),clusterfull) >= minClustSize);
%         
%         if size(clus,2)==0
%             ClusterView=[0,0]
%             clus=0;
%         elseif size(clus,2)==1
%             ClusterView=[J_C1,1,size(clus,1)];
%         else
%             for ic=1:1:size(clus,2)
%                 ClusterView(ic,:)=[J_C1,ic,size(clus{1,ic},1)];
%             end
%         end
%         
%         
%         clus_All{ J_C1,1}=clus;
%         ClusterView1_All{ J_C1,1}=ClusterView;
%     end
%     
% end
% %----------------Select Good clusters----------
% %----------------Classify all clusters----------
% 
% toc
% Time_Select=toc;
% tic
% %------------------Connect------------------
% ClusterViewMatrix=cell2mat(ClusterView1_All);
% [maxrow,maxrowposition]=max(ClusterViewMatrix(:,3));
% maxR=ClusterViewMatrix(maxrowposition,1);
% maxC=ClusterViewMatrix(maxrowposition,2);
% clus_initial=clus_All{maxR,1}{1,maxC};                   %%%%%%%%%%%%%main cluster to connect all possible clusters
% [TVPick, ReorderParticles, Rpick,tpick] = ConnectParticlesInDifferentClusterWithSigma(limit,Repeattimes,clus_All,clus_initial,maxR,R_All,t_All,TV_All,Particles1);
% %------------------Connect------------------
% disp('Connected finished');
% toc
% Time_Connect=toc;
% TimeCC=Time_Connect+Time_Classify+Time_Select;  TimeAll=TimeCC+Time_JRMPC;
% time=TimeAll
% effectiveParticlesNumber=size(TVPick,1);%number of particles included in the final results.
% 
% 
% % %Draw figure
% data2c=cell2mat(TVPick')';
% idx = find(data2c(:,1) <  sizeview& ...
%     data2c(:,1) > -sizeview  & ...
%     data2c(:,2) <  sizeview & ...
%     data2c(:,2) > -sizeview  & ...
%     data2c(:,3) <  sizeview & ...
%     data2c(:,3) > -sizeview);
% data2c = data2c(idx,:);
% % densityc=visualizeCloud3D2(data2c,10,0);
% % figure()
% % hold on
% % % 
% % % 
% % scatter3(data2c(:,1),data2c(:,2), data2c(:,3),1,densityc,'.');
% % title(sprintf('TV Number of particles=%d',effectiveParticlesNumber), 'fontweight','bold','fontsize',12,'Color', 'w');
% % colormap(hot);
% % colordef black
% % hold off
% 
% 
% %%%%%%%%%Generate Gif figure and save it
% %             filenameTVpick=[ model_name  'K=' num2str(K)    'Time=' num2str(time)  '.gif' ];
% %             for i=1:36
% %                 camorbit(10,0,'camera')
% %                 drawnow
% %                 M=getframe(gcf);
% %                 nn=frame2im(M);
% %                 [imind,cm]=rgb2ind(nn,256);
% %                 if i == 1
% %                     imwrite(imind,cm,filenameTVpick,'gif', 'Loopcount',inf);
% %
% %                 else
% %                     imwrite(imind,cm,filenameTVpick,'gif','WriteMode','append');
% %
% %                 end
% %             end
% 
% %%%%%%%%%%%%%%%Save all the data
% %             filename=[ model_name  'K=' num2str(K)    'S=' num2str(Sa) 'Time=' num2str(time) 'Size' num2str(effectiveParticlesNumber) '.mat' ];
% %             save(filename)
% %
% %         close all

%% JRMPC 2D

clc
clear
dip_initialise % Initialize Diplib
%%Load data
addpath(genpath('2D_JRMPC'))



USE_GPU_GAUSSTRANSFORM = 1;
USE_GPU_EXPDIST = 1;

load('tud_442particles_dol50.mat')
CCD_pixelsize=130;     %CCD_pixelsize for the data
Repeattimes=2;           %Number of initializations of the JRMPC
raster_spacing=5;        
ParticleNumber=size(particles,2);%Number of input particles
M=ParticleNumber;

[Particles1,V1,meanlocs] =PrepareInputParticles(particles,CCD_pixelsize);
%Particles1:Particles with uncertainties and coordinates
%V1:particles' coordinates
%mean locs: average number of localizations in the particles



%Step1: Joint registration
Run_JRMPC  %We can change Xin and JRMPC parameters inside

%Step2: Dissimilarity Matrix Calculation
Run_DissimilarityMatrixCalculation %no need to change anything inside

%Two parameters defined for the classification
nc=2; %number of clusters
cluster_mode=2; %2: MDS clustering, 1: Hierarchical agglomerative clustering approach as alternative to MDS
minClustSize=M/(nc+1);  %The minumum number of  the particle 
%Step3: Classification
Run_Classification %We can set different nc,minClustSize before Run_Classification

%Step4: Connection
Run_Connection    %no need to change anything inside

time_All=time_JRMPC+time_Dissimilarity+time_Connection+time_Classification; % Total Computational Time

%Step5: FRC resolution measurement
%%FRC resolution measurement installation needed
%%Used to calculate FRC resolution See https://github.com/imphys/FRC_resolution
%Run_MainClusterFRC

%Draw the density Plot
XX=[];
pixelnum=300;%number of pixels in the density plot
diameter=100;%[nm]
XX=cell2mat(TVPick')';%final localizations
nef=size(TVPick,1);%number of particles in the final reconstruction
lo=size(XX,1);%number of localizationss in the final reconstruction
%Diplib needed
titlename= ['N=' num2str(nef) 'time=' num2str(time_All) ];

[dip, Z] = visualizeCloud2DW(XX,pixelnum,diameter,titlename);%Draw the density plot



