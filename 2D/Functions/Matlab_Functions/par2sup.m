function [sup]=par2sup(particles,range)


for i=1:range
sup{1,i}.points=[];
sup{1,i}.sigma=[];
end


for i=range
    
sup{1,i}.points=[sup{1,i}.points;particles{1,i}.points];
sup{1,i}.sigma=[sup{1,i}.sigma;particles{1,i}.sigma];

end
end
