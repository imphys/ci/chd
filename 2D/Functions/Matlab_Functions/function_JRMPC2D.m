function [registered_chunk]=function_JRMPC2D(Particles,Use_CPU)

 addpath(genpath('2D_JRMPC'))
 
 
 particles=Particles;
 Use_CPU=Use_CPU;
CCD_pixelsize=130;     %CCD_pixelsize for the data
Repeattimes=5;           %Number of initializations of the JRMPC
raster_spacing=5;        
ParticleNumber=size(particles,2);%Number of input particles
M=ParticleNumber;
    particles=particles(~cellfun('isempty',particles));

[Particles1,V1,meanlocs] =PrepareInputParticles(particles,CCD_pixelsize);
%Particles1:Particles with uncertainties and coordinates
%V1:particles' coordinates
%mean locs: average number of localizations in the particles



%Step1: Joint registration
Run_JRMPC  %We can change Xin and JRMPC parameters inside %%changed from 17 to 8

%Step2: Dissimilarity Matrix Calculation
Run_DissimilarityMatrixCalculation %no need to change anything inside


%Two parameters defined for the classification
nc=2; %number of clusters for from 2 to 20
cluster_mode=2; %2: MDS clustering, 1: Hierarchical agglomerative clustering approach as alternative to MDS
minClustSize=M/(nc+1);  %The minumum number of  the particle 
minClustSize=1;  %The minumum number of  the particle 

%Step3: Classification
 Run_Classification %We can set different nc,minClustSize before Run_Classification

%Step4: Connection
Run_ConnectionWithSigma   %no need to change anything inside

time_All=time_JRMPC+time_Dissimilarity+time_Connection+time_Classification; % Total Computational Time


for i=1:length(TVPick)
    registered_chunk{1,i}.points= TVPick{i,1}';
    registered_chunk{1,i}.sigma= ParticlesPick{1,i}.sigma;
end


end