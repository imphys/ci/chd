
function [chunks]=Isopca_chunks(subParticles,numc,idx)
 q=ceil(length(idx)/numc)-1;
for i=1:length(idx)
    reorderparticles{i}=subParticles{1,idx(i)};
end

j=1;
c=0;
for i=1:length(idx)
    c=c+1;
        chunks{j,c}=reorderparticles{i};
if mod(i,q+1)==0
    j=j+1;
    c=0;
end

end
