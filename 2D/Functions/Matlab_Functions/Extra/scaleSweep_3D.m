%% Scale sweep

addpath(genpath('3D_All2All'))
addpath(genpath('Data'))
USE_GPU_GAUSSTRANSFORM = 1;
USE_GPU_EXPDIST = 1;
initAng = 'grid_72.qua';

load('/home/shaghparast/ISO_PCA_NEW/Straightened_NPC_1719_Corrected_and_Uncorrected.mat')

subParticles=Straightened_NPC_Corrected;
% scales = linspace(1,60,60);
scales = linspace(0.01,10,60);

for t = 1:10
    order = randperm(1719);  %shuffle all particles (change 372 to number of particles)
    idxM = order(1);
    idxS = order(2);  

    M = subParticles{idxM};
    S = subParticles{idxS};
    
    M.points = double(M.points);
    S.points = double(S.points);
    M.sigma = double(M.sigma);
    S.sigma = double(S.sigma); 

    parfor i = 1:length(scales) %was parfor
        [t,i]
        [ ~, val(t,i)] = pairFitting3D(M.points, S.points,   M.sigma,   S.sigma,scales(i), initAng, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST);
    end
end

val_norm = val./repmat(max(val,[],2),1,length(scales)); 

figure()
for i = 1:size(val,1)
    plot(scales,val_norm(i,:))
    hold on
end
hold on
plot(scales,mean(val_norm,1),'k','LineWidth',5)
xlabel('scale (pixels)')
ylabel('costValue (norm. per line)')
title('Scale Sweep')
grid; grid minor