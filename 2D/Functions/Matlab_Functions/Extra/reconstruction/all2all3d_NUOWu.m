%%
% (C) Copyright 2018-2020
% Faculty of Applied Sciences
% Delft University of Technology
%
% Hamidreza Heydarian, November 2020.
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%    http://www.apache.org/licenses/LICENSE-2.0

%%
close all
clear all

% add the required dir to path. Choose the right code block for your OS
% for linux
path_mex_matlab1 = genpath('build/mex/');
path_mex_matlab2 = genpath('build/figtree/src/mex/');
path_matlab = genpath('MATLAB');
path_data=genpath('data')

% for windows
% path_mex_matlab1 = genpath('build\Debug\mex');
% path_mex_matlab2 = genpath('build\figtree\Debug');
% path_matlab = genpath('MATLAB');

addpath(path_mex_matlab1)
addpath(path_mex_matlab2)
addpath(path_matlab)
addpath(path_data)
% CPU/GPU settings (CPU = 0, GPU = 1)
USE_GPU_GAUSSTRANSFORM = 1;
USE_GPU_EXPDIST = 1;
typeset=[5,6]
% load dataset stored in data directory
for type=typeset
    Particles=[];
    if type==1
        load('ParticlesNUP1.mat');
        %Sigma =1 and don't need CCD_pixels
        CCD_pixelsize=130;
        dol=0.4;
        Particles=particles;
        numParticles=size(Particles);
        %             dimention=ceil(numParticles(1,2)/5);
        for i=1:numParticles(1,2)
            Particles{1,i}.points(:,1:3)= Particles{1,i}.points(:,1:3)./CCD_pixelsize;
            Particles{1,i}.sigma(:,1:2)=   Particles{1,i}.sigma(:,1:2)./CCD_pixelsize;
        end
        
        model_name=[num2str(numParticles(1,2)),'ParticlesNUP1']
    end
    if type==2
        load('ParticlesNUP2.mat');%707, 150
        %Sigma =1 and don't need CCD_pixels
        CCD_pixelsize=130;
        dol=0.4;
        Particles=particles;
        numParticles=size(Particles);
        %   dimention=ceil(numParticles/4)(1,2);
        for i=1:numParticles(1,2)
            Particles{1,i}.points(:,1:3)= Particles{1,i}.points(:,1:3)./CCD_pixelsize;
            Particles{1,i}.sigma(:,1:2)=   Particles{1,i}.sigma(:,1:2)./CCD_pixelsize;
        end
        
        model_name=[num2str(numParticles(1,2)),'ParticlesNUP2']
    end
    
    if type==3
        load('ParticlesNUP3.mat');
        %Sigma =1 and don't need CCD_pixels
        CCD_pixelsize=130;
        dol=0.4;
        Particles=particles;
        numParticles=size(Particles);
        %             dimention=ceil(numParticles/5);%150
        for i=1:numParticles(1,2)
            Particles{1,i}.points(:,1:3)= Particles{1,i}.points(:,1:3)./CCD_pixelsize;
            Particles{1,i}.sigma(:,1:2)=   Particles{1,i}.sigma(:,1:2)./CCD_pixelsize;
        end
        
        model_name=[num2str(numParticles(1,2)),'ParticlesNUP3']
    end
    
    if type==4
        load('ParticlesNUP4.mat');
        %Sigma =1 and don't need CCD_pixels
        CCD_pixelsize=130;
        dol=0.4;
        Particles=particles;
        numParticles=size(Particles);
        % dimention=ceil(numParticles/5);
        for i=1:numParticles(1,2)
            Particles{1,i}.points(:,1:3)= Particles{1,i}.points(:,1:3)./CCD_pixelsize;
            Particles{1,i}.sigma(:,1:2)=   Particles{1,i}.sigma(:,1:2)./CCD_pixelsize;
        end
        
        model_name=[num2str(numParticles(1,2)),'ParticlesNUP4']
    end
    if type==5
        load('ParticlesNUP5.mat');
        %Sigma =1 and don't need CCD_pixels
        CCD_pixelsize=130;
        dol=0.4;
        Particles=particles;
        numParticles=size(Particles);
        
        for i=1:numParticles(1,2)
            Particles{1,i}.points(:,1:3)= Particles{1,i}.points(:,1:3)./CCD_pixelsize;
            Particles{1,i}.sigma(:,1:2)=   Particles{1,i}.sigma(:,1:2)./CCD_pixelsize;
        end
        
        model_name=[num2str(numParticles(1,2)),'ParticlesNUP5']
    end
    if type==6
        load('ParticlesNUPAll.mat');
        %Sigma =1 and don't need CCD_pixels
        CCD_pixelsize=130;
        dol=0.4;
        Particles=particlesAll;
        numParticles=size(Particles);
        %         dimention=ceil(numParticles(1,2)/5);
        for i=1:numParticles(1,2)
            Particles{1,i}.points(:,1:3)= Particles{1,i}.points(:,1:3)./CCD_pixelsize;
            Particles{1,i}.sigma(:,1:2)=   Particles{1,i}.sigma(:,1:2)./CCD_pixelsize;
        end
        
        model_name=[num2str(numParticles(1,2)),'ParticlesNUPAll']
    end
    tic
    CCD_pixel=130;
    % choose N particles
%     if N > numel(particles)
        N = numel(particles);
%     end
    subParticles = cell(1,N);
    
    for i=1:N
        subParticles{1,i}.points = (particles{1,i}.points-mean(particles{1,i}.points))./CCD_pixel;
        %     idxZ = find(subParticles{1,i}.points(:,3) > 1 | subParticles{1,i}.points(:,3) < -1);
        %     subParticles{1,i}.points(idxZ,:) = [];
        subParticles{1,i}.sigma = particles{1,i}.sigma./CCD_pixel.^2;
        %     subParticles{1,i}.sigma(idxZ,:) = [];
    end
    toc
    %% STEP 1
    % all-to-all registration
    
    % when the particles have a prefered orientation, like NPCs that lie in the
    % cell membrane, it is recommanded to do in-plane initialization to save
    % computational time for example initAng = 1 (see pairFitting3D.m line 68).
    initAng = 1;%'grid_72.qua';
    
    % For NPC particle scale = 0.1 (10 nm) is the optimal choice. In other
    % cases, it is recommanded to use the scale_sweep() function to find the
    % optimal value. This needs to be done once for a structure (see Online
    % Methods and demo2.m)
    scale = 0.1;    % in camera pixel unit (corresponds to 10 nm in physical unit)
    
    disp('all2all registration started!');
    [RR, I] = all2all3D(subParticles, scale, initAng, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST);
    disp('all2all registration finished!');
    %% STEP 2
    % iterations of:
    % 2-1 lie-algebra averaging of relative transformation
    % 2-2 consistency check
    % 2-3 constructing the data-driven template
    
    nIterations = 5;           % number of lie-algebra avg and consistency check
    % iterations 5 is enough
    flagVisualizeSijHist = 1;   % show S_ij histogram (boolean)
    threshold = 0.5;            % consistency check threshold. described it in the paper size of the particles
    [initAlignedParticles, sup] = relative2absolute(subParticles, RR, I, N, ...
        nIterations, threshold, 1);
    
    %% STEP 3
    % bootstrapping with imposing symmetry prior knowledge
    USE_SYMMETRY = 1;   % flag for imposing symmetry prio knowledge
    M1 = [];            % not implemented
    iter = 5;           % number of iterations
    [superParticleWithPK, ~] = one2all3D(initAlignedParticles, iter, M1, '.', sup, USE_SYMMETRY, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST);
    
    %% STEP 3
    % bootstrapping without imposing symmetry prior knowledge
    USE_SYMMETRY = 0;   % flag for imposing symmetry prio knowledge
    M1=[];              % not implemented
    iter = 5;           % number of iterations
    [superParticleWithoutPK, ~] = one2all3D(initAlignedParticles, iter, M1, '.', sup, USE_SYMMETRY, USE_GPU_GAUSSTRANSFORM, USE_GPU_EXPDIST);
  toc
    time=toc;
    %% Visualize the results
    visualizeSMLM3D(superParticleWithoutPK{1,5},0.05, 1);
     visualizeCloud3D(superParticleWithPK{1,5},0.05, 1);
    filename=[ ' model_name  Time=' num2str(time) '.mat' ];
    save(filename)
end