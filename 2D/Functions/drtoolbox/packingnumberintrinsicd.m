 
X=mds;
% Parameters for the estimation
            r(1) = 1; r(2) = 120;
            epsilon = 2;
            max_iter = 200;
            done = 0;
            l = 0;
            
            % Perform iterations (until 'convergence')
            while ~done
                l = l + 1;
                perm = randperm(size(X, 1));
                
                % Compute L for two radiuses (size of C is packing number)
                for k=1:2
                   C = [];
                   for i=1:size(X, 1)
                       for j=1:numel(C)
                           if sqrt(sum((X(perm(i),:) - X(C(j),:)) .^ 2)) < r(k)
                               j = size(X, 1) + 1;
                               break;
                           end
                       end
                       if numel(C) == 0 || j < size(X, 1) + 1 
                           C = [C; perm(i)];
                       end
                   end
                   L(k, l) = log(numel(C));                 % maximum cardinality of an r(k)-separated subset of X
                end
                                
                % Estimate of intrinsic dimension
                no_dims = -((mean(L(2,:)) - mean(L(1,:))) / (log(r(2)) - log(r(1))));
                
                % Stop condition
                if l > 10
                    if 1.65 * (sqrt(var(L(1,:)) .^ 2 + var(L(2,:)) .^ 2) / (sqrt(l) * log(r(2)) - log(r(1)))) < no_dims * ((1 - epsilon) / 2)
                        done = 1;
                    end
                end
                if l > max_iter
                    done = 1;
                end
            end