  % Set neighborhood range to search in
           
  
  k1 = 6;
            k2 = 12;
            
            % Compute nearest neighbor dimension estimation
            [D, ind] = find_nn(X, k2);
            Tk = zeros(1, k2 - k1);
            for k=k1:k2
                Tk(k - k1 + 1) = sum(full(D(sub2ind(size(D), (1:size(X, 1))', double(ind(:,k))))));
            end
            Tk = Tk ./ size(X, 1);
            no_dims = (log(Tk(end)) - log(Tk(1))) / (log(k2) - log(k1));
            