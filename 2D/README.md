# Continuous heterogeneity detection

This package detects continuous heterogeneity in single molecule localization data. The method can be used to identify structural or biological heterogeneity which results in more faithful particle fusion and reconstruction. The method can be applied on both 2D and 3D localization data. It directly works on localization data and it is template free. The pipeline is composed of four steps:

1. Pairwise registration of particles.
1. Multi dimensional scaling space representation.
1. Unrolling data using Isomap and PCA to create latent space.
1. Binning particles and reconstruction per bin.

The main code is written in MATLAB and some of the compute-intensive kernels have been written in CUDA and C++.

## Requirements

* [MATLAB](https://www.mathworks.com/products/matlab.html) (version>2019b)
* [Dipimage](http://www.diplib.org/) (version>2.9)
* [Cuda](https://developer.nvidia.com/cuda-downloads) (version>11.2)
* [GCC](https://gcc.gnu.org/) (version>5.5.0)
* [CUB](https://nvlabs.github.io/cub/) (version>1.8.0)
* [Cmake](https://cmake.org/) (version>3.14.3)

Running the demo script in Matlab requires the installation of the following toolboxes in Matlab R2021b:

* Image processing toolbox
* Statistics and machine learning
* Optimization toolbox

## Operating system

The recommended OS to run this package is linux. For windows installation please see the documentation to install requirements on this [link](https://github.com/imphys/smlm_datafusion3d).

## Installation of 2D version

1. Download the software or clone it using the code below:

   ```bash
   git clone https://gitlab.tudelft.nl/imphys/ci/chd.git
   ```

1. Install the requirements. If you plan to use GPU, the correct version of the Cuda and CUB must be installed. Make sure you are using gcc version is higher than 5.

    Use the following commands to build the necessary libraries for this software:

```bash
cd SOURCE_DIRECTORY
cmake .
make
make install
````

   If you do not have a CUDA capable GPU you could install the software without GPU acceleration. Do note that the code will be orders of magnitude slower. Use the following commands to install the CPU-only version:


   ```bash
   cmake -DUSE_GPU=OFF .
   make
   make install
   ```

   CMake locates the code's dependencies and generates a Makefile. Make compiles the mex files and necessary shared libraries. Make install copies the mex files to the right directory for use in Matlab, it does not require privileges to run. For further information please see [here](https://github.com/imphys/smlm_datafusion3d) for the instruction.

1. To use this package it is required to have Mex files (`mex_expdist`) ready on your device. Each package (2D,3D) requires its own MEX files to be used in Matlab. On the command line it is needed to include the path where the compiled libraries (`.so`) are located in `LD_LIBRARY_PATH` and in Matlab the path where the compiled mex-files (`.mexa64`)  are located needed to be added with `addpath`. Please note, once you need to use other package (2D,3D) you need to reassign new Mex files to the linux path. For adding the mex files into linux path you should put your software directory to `<CHD_DIR>` in the codes below.

   ```bash
   export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:<CHD_DIR>/2D
   ```

1. Start MATLAB and set `Use_CPU = 0` or `Use_CPU = 1` based on the hardware you choose. Run demo codes. Note that when using the demo script `Demo_2d.m` the variable `Use_CPU` must be set inside the script before running it!



# Installation of 3D version

### Compile the code
In the following

- SOURCE_DIRECTORY is the root directory of the sources
- CUB_DIRECTORY is the root directory of the downloaded [CUB library](https://nvlabs.github.io/cub/) sources


Use the following commands to build the necessary libraries for this software:

```bash
cd SOURCE_DIRECTORY/3D
mkdir build
cd build
cmake -DCUB_ROOT_DIR=CUB_DIRECTORY SOURCE_DIRECTORY/3D
make
````
### Use the code
Next, we need to locate the built libraries for MATLAB:
```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:<CHD_DIR>/3D/build/mex

``` 
Then, run MATLAB and set USE_GPU parameters to 0 or 1 whether you plan to use GPU or not and run the demo script.


# Examples

As an example of using the package we provided 2 Demo files. 2D_Demo which detects continous structural heterogenity of NPC structures and performs a reconstruction using this technique. 3D_Demo detects continous structural heterogeneity on 3D tetrahedron dataset and performs reconstruction per bin using Joint registeration of multiple pointclouds.

# Acknowledgements


We reused and adapted some files from the following sources:

[1] Huijben, Teun APM, et al. "Detecting structural heterogeneity in single-molecule localization microscopy data." Nature communications 12.1 (2021): 3791.

[2] Heydarian, Hamidreza, et al. "Template-free 2D particle fusion in localization microscopy." Nature methods 15.10 (2018): 781-784.

[3] Wang, Wenxiu, et al. "Joint registration of multiple point clouds for fast particle fusion in localization microscopy." Bioinformatics 38.12 (2022): 3281-3287.

[4] Heydarian, Hamidreza, et al. "3D particle averaging and detection of macromolecular symmetry in localization microscopy." Nature Communications 12.1 (2021): 2847.

[drtoolbox](https://lvdmaaten.github.io/drtoolbox/)

## Reference

If you find this code useful for your research, please cite our paper(-). For any other matters regarding the code please open a topic in issues or send email to [(s.haghparast@tudelft.nl)](s.haghparast@tudelft.nl).
