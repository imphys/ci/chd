% This code loads a dataset from the '/data' directory 
% which should be a cell array of structures called 
% subParticles with localizations in the 'point' field 
% [x,y] and SQUARED uncertainties in 'sigma' field.The estimated size of the NPC is 
% stored in 'size' field:
%   
% subParticles{1}.points  -> localization data (x,y) in camera pixel units
% subParticles{1}.sigma   -> localization uncertainties (sigma) in SQUARED pixel units
% subParticles{1}.size   ->  Particle size (NPC radius) in nm
%
% The following code will load the data from file and 
% then performs 4 steps to detect continous structural heterogeneity.
% Different example datasets are provided, both experimental as simulated data. 
% You only need to provide the 'dataset' name, and optionaly the values for
% K (number of neighbours used in Isomap).
% 
%
% The code makes use of the parallel computing toolbox 
% to distribute the load over different workers. 
% 
% (C) Copyright 2023               Quantitative Imaging Group
%     All rights reserved          Faculty of Applied Physics
%                                  Delft University of Technology
%                                  Lorentzweg 1
%                                  2628 CJ Delft
%                                  The Netherlands
%
% Sobhan Haghparast, 2023
%%  
close all
clear all
clc
%%%
% Set this variable to select using the CPU or GPU
%  if value is 1 the CPU will be used
%  if value is 0 the GPU will be used
Use_CPU = 1;

% add the required directory to path
addpath(genpath('MATLAB'))
addpath(genpath('Functions'))   
addpath(genpath('data'))

load('2D_NPCs.mat')

dataset = '2D_NPCs';           % -- Choose the dataset name that exists in data folder --
N = length(subParticles);      % -- choose number of particles --

disp('Step 1: Pairwise registration of particles')

outdir = ['output/' dataset];
if ~exist(outdir,'dir')
    mkdir(outdir);
else
    disp('WARNING: outdir already exists')
end    
save([outdir '/subParticles'], 'subParticles');
scale = 0.045;                 % -- Scale parameter --
CCD_Pixel=130;
if (Use_CPU == 1)
    disp("Using CPU")
else
    disp("Using GPU")
end
% all2all registration
all2all(subParticles, [outdir '/all2all_matrix'], scale,Use_CPU);      

% Fusion of all particles (not necessary for continous heterogeneity detection)
[initAlignedParticles, M1] = outlier_removal(subParticles, [outdir '/all2all_matrix/'], outdir);        % -- Lie-algebraic averaging --


% Making dissimilarity matrix using pairwise distances 
[SimMatrix, SimMatrixNorm] = MakeMatrix(outdir,subParticles,N);  
D = SimMatrixNorm+SimMatrixNorm';                   % -- convert upper-triangular matrix to full similarity matrix --
D = max(D(:))-D;                                    % -- convert to dissimilarity --
D = D - diag(diag(D));    % set diagonal to 0

disp('Step 2: Multi dimensional scaling space representation')

dimension_number= 30 ;                                             % -- choose number of dimensions --
mds = mdscale(D,dimension_number,'Criterion','metricstress');     % -- perform multi-dimensional scaling --

% Extract NPC sizes to single vector (For experimental datasets create size vector using size estimation function provided in matlab functions)

    for i=1:N
         Size(i)=subParticles{1,i}.size;
    end

% Show first three dimensions of MDS 
figure, scatter3(mds(:,1),mds(:,2),mds(:,3),30, Size,'o','filled'), title 'Multidimensional Scaling' 
colorbar
colormap(jet)

disp('Step 3: Unrolling data using Isomap and PCA to create latent space')

% Unrolling data using ISOMAP
addpath (genpath('Functions'))   % -- Adding dimension reduction toolbox to matlab path --
k=8;                                                      % -- Number of the neighbours selected to perform unrolling --
[Unrolled, mapping] = isomap(mds, size(mds,2), k);         % -- Performs isomap--

figure,scatter3(Unrolled(:,1),Unrolled(:,2),Unrolled(:,3),70, Size,'filled');,title('Unrolled data using ISOMAP') % -- visualizing first 3 dimensions of the unrolled manifold --
axis equal
colorbar
colormap(jet)
[U S V]=svd(Unrolled);                                      % -- Performing singular value decomposition --

Latent_space=U*S(:,1)*transpose(V(1,1));                 % -- Projecting data to its principal component axis to form the latent space --

figure,histogram(Latent_space,10),title('Histogram of the latent space');                    % -- Visualizing the histogram of the latent space --

svd_unrolled=svd(Unrolled);                               % -- Generating all eigen values --
figure,scatter(1:1:30,svd_unrolled,50,'filled'),title('SVD of the mapped data'), xlabel('dimension'),ylabel('SVD') % -- Visualization of eigen values on each axis 

% Explained variance
rmpath (genpath('Functions/drtoolbox')) % -- To avoid confliction between different pca funstions in drtoolbox and matlab built in function, drtoolbox must be removed from Matlab path--

[coeff,score,latent,tsquared,explained] = pca(Unrolled); % -- Generates variance explained on each PC axis --
figure,bar(explained,1),title('Variance explained'),xlabel('Dimensions'),ylabel('Variance explained') % -- Visualization of the variance explained --

% sort lowd embed
[out,idx] = sort(Latent_space) ;% -- reordering the particles based on their value in latent space --

disp('Step 4: Binning particles and reconstruction per bin')

Number_of_divisions=10 ;% -- defines the number of divisions --

% Registeration of particles in each bin using 2D JRMPC
registered={};

    for i=1:Number_of_divisions
        registered(i,:)=function_JRMPC2D(subParticles(1,idx((Number_of_divisions*(i-1))+1:Number_of_divisions*i)), Use_CPU);
    end
    
registered_bin_cloud=[];

    for i=1:Number_of_divisions
        ctemp=registered(i,:);
        ctemp=ctemp(~cellfun('isempty',ctemp));
        registered_bin_cloud{1,i}=par2cloud(ctemp,1:length(ctemp));
    end
        
% Final reconstructions per bin in scatter plot (Color coded based on the radius size)
%% Figures 
% You can choose different visualizations for the code 
% The default is  using renderprojection function.
% You can uncomment the codes below to use other visualizations.
% Plots using render projection function
    for i=1:Number_of_divisions
        subplot(2,5,i)
        fig=renderprojection(registered_bin_cloud{1,i}(:,1)-mean(registered_bin_cloud{1,i}(:,1)),registered_bin_cloud{1,i}(:,2)-mean(registered_bin_cloud{1,i}(:,2)),zeros(length(registered_bin_cloud{1,i}(:,1)),1),180,0, [-80 80],[-80 80],1,1, 1, 1);
        title(['Bin' num2str(i)])
        hold on
        plot(  [130; 150], [150; 150], '-w', 'LineWidth', 4)
        text(140,140, '20 nm','color','w', 'HorizontalAlignment','center')
        axis off
        axis equal
    
    end

% color=jet(100);
% for i=1:Number_of_divisions
%     subplot(2,5,i)
%         for j=1:N/Number_of_divisions
%             scatter(registered{i,j}.points(:,1)-mean(registered{i,j}.points(:,1)),registered{i,j}.points(:,2)-mean(registered{i,j}.points(:,2)),10,color(N/Number_of_divisions*i,:),'filled','o')
%             hold on
%         end
%     axis equal
%     xlim([-100 100])
%     ylim([-100 100])
% 
% end

        
% % Plots using dipimage visualization 
%     dip_initialise; % it is needed for dipimage 2.9 or less
%     for i=1:Number_of_divisions
%         visualizeCloud2D(registered_bin_cloud{1,i}./CCD_Pixel, 600, 2, 0);

